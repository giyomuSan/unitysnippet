﻿using UnityEngine;
using System.Collections;

public class UpdateViaUpdateCallback : AIControllerComponent {

	[SerializeField] float _updateFrequency;

	private float _timer;

	// artificially create a timer to handle updates
	void Update () {
		_timer += Time.deltaTime;
		if (_timer > _updateFrequency) {
			_timer -= _updateFrequency;
			ProcessAI();
		}
	}
}
