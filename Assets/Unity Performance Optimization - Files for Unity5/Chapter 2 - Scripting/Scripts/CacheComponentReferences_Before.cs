﻿using UnityEngine;
using System.Collections;

public class CacheComponentReferences_Before : MonoBehaviour {

	void TakeDamage() {
		if (GetComponent<HealthComponent>().health < 0) {
			GetComponent<Rigidbody>().detectCollisions = false;
			GetComponent<Collider>().enabled = false;
			GetComponent<AIControllerComponent>().enabled = false;
			GetComponent<Animator>().SetTrigger("death");
		}
	}

}
