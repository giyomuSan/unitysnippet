﻿using UnityEngine;
using System.Collections;

// Note: This code varies slightly from the code in the book. 
// This version disables an assigned object, rather than deactivating the Component.

public class DisableObjectsByDistance : MonoBehaviour {

	[SerializeField] GameObject _target;
	[SerializeField] float _maxDistance;
	[SerializeField] int _coroutineFrequency;
	[SerializeField] string _objectName;
	[SerializeField] GameObject _objectToDisable;
	
	void Start() {
		StartCoroutine(DisableAtADistance());
	}
	
	IEnumerator DisableAtADistance() {
		while(true) {
			float distSqrd = (transform.position - _target.transform.position).sqrMagnitude;
			

			if (distSqrd < _maxDistance * _maxDistance) {
				if (!_objectToDisable.activeSelf) {
					Debug.Log (string.Format ("{0} enabled", _objectName));
					_objectToDisable.SetActive(true);
				}
			} else {
				if (_objectToDisable.activeSelf) {
					Debug.Log (string.Format ("{0} disabled", _objectName));
					_objectToDisable.SetActive(false);
				}
			}
			
			for (int i = 0; i < _coroutineFrequency; ++i) {
				yield return new WaitForEndOfFrame();
			}
		}
	}

}
