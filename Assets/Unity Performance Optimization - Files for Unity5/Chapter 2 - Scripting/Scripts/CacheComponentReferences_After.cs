﻿using UnityEngine;
using System.Collections;

public class CacheComponentReferences_After : MonoBehaviour {

	// private variables to store cached references
	private HealthComponent _healthComponent;
	private Rigidbody _rigidbody;
	private Collider _collider;
	private AIControllerComponent _aiController;
	private Animator _animator;

	// cache the references during initialization
	void Awake() {
		_healthComponent = GetComponent<HealthComponent>();
		_rigidbody = GetComponent<Rigidbody>();
		_collider = GetComponent<Collider>();
		_aiController = GetComponent<AIControllerComponent>();
		_animator = GetComponent<Animator>();
	}

	// use the cached references at runtime
	void TakeDamage() {
		if (_healthComponent.health < 0) {
			_rigidbody.detectCollisions = false;
			_collider.enabled = false;
			_aiController.enabled = false;
			_animator.SetTrigger("death");
		}
	}

}
