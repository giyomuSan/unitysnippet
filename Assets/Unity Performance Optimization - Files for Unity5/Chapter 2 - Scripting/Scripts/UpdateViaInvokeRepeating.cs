﻿using UnityEngine;
using System.Collections;

public class UpdateViaInvokeRepeating : AIControllerComponent {

	[SerializeField] float _aiUpdateFrequency;

	void Start() {
		InvokeRepeating("ProcessAI", 0f, _aiUpdateFrequency);
	}

}
