﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct GameObjectList {
	public string name;
	public List<GameObject> _objects;
}

public class StaticBatchTester : MonoBehaviour {

	[SerializeField] List<GameObjectList> _objectList;

	void Update() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			BatchObjects();
		}
	}

	void BatchObjects() {
		for(int i = 0; i < _objectList.Count; ++i) {
			GameObjectList list = _objectList[i];
			for(int j = 0; j < list._objects.Count; j++) {
				StaticBatchingUtility.Combine (list._objects.ToArray(), gameObject);
			}
		}
	}
}
