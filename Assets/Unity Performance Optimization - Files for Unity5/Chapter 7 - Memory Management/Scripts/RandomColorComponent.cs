﻿using UnityEngine;
using System.Collections;

public class RandomColorComponent : PoolableComponent {

	[SerializeField] Renderer _renderer;

	public override void Spawned ()
	{
		base.Spawned();
		_renderer.material.color = new Color(Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
	}

	public override void Despawned ()
	{
		base.Despawned();
	}
}
