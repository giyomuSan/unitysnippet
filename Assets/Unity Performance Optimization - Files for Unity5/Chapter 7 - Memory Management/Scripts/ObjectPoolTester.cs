﻿using UnityEngine;
using System.Collections;

public class TestObject : IPoolableObject {
	public void New() {
		// very first initialization here
	}
	public void Respawn() {
		// reset data which allows the object to be recycled here
	}

	public int Test(int num) {
		return num * 2;
	}
}


public class ObjectPoolTester : MonoBehaviour {

	private ObjectPool<TestObject> _objectPool = new ObjectPool<TestObject>(100);
	
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			_objectPool.Reset ();
			int sum = 0;
			for(int i = 0; i < 100; ++i) {
				TestObject obj = _objectPool.Spawn ();
				sum += obj.Test(i);
			}
			//Debug.Log (string.Format ("(Sum 1-to-100) *2 = {0}", sum));
		}
	}
}
