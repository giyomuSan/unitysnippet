﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Note: This approach loads the AudioClip using Resources.Load() dynamically.
// Once the resource is no longer needed (the audio clip has stopped playing), the
// resource is freed from memory via (Resources.UnloadAsset()), reducing runtime
// memory consumption

// this system can be extended to support multiple audio sources, and audio clips playing
// simultaneously (this is how a lot of Audio assets on the Asset Store manage resources)

public class AudioSystem : SingletonAsComponent<AudioSystem> {
	
	[SerializeField] AudioSource _source;
	
	AudioClip _loadedResource;

	// NOTE: Because this SingletonAsComponent needs outside references (an AudioSource)
	// it must exist in the Scene prior to startup. Hence, the ShouldBeSetToDontDestroyOnLoadDuringAwake
	// method is overridden to ensure it remains if another scene is loaded
	protected override bool ShouldBeSetToDontDestroyOnLoadDuringAwake ()
	{
		return true;
	}
	
	public static AudioSystem Instance {
		get { return ((AudioSystem)_Instance); } 
		set { _Instance = value; }
	}
	
	public void PlaySound(string resourceName) {
		if (!_source.isPlaying) {
			_loadedResource = Resources.Load (resourceName) as AudioClip;
			_source.PlayOneShot (_loadedResource);
		}
	}
	
	void Update() {
		if (!_source.isPlaying && _loadedResource != null) {
			Resources.UnloadAsset(_loadedResource);
			_loadedResource = null;
		}
	}
}
