﻿using UnityEngine;
using System.Collections;

// Note: Because this component relies on an Audio Source that has
// a pre-existing reference to an Audio Clip, the entire clip will always
// exist in memory until the Scene ends.

public class AudioSystemTest : MonoBehaviour {

	[SerializeField] AudioSource _source;

	void Update() {
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			AudioSystem.Instance.PlaySound("TestSound");
		}

		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			_source.PlayOneShot(_source.clip);
		}
	}
}
