﻿using UnityEngine;
using System.Collections;

public class CameraRotate : MonoBehaviour {

	public bool Up = false;
	public bool Forward = true;
	public bool Right = false;
	public float Speed = 0.025f;
	public bool IndependentOfPause;
	
	void Update()
	{
		float thisSpeed = Speed * Time.deltaTime;
		
		if (IndependentOfPause) {
			thisSpeed *= 1 / Time.timeScale;
		}
		
		if (Up)
			transform.Rotate(Vector3.up, thisSpeed);
		if (Forward)
			transform.Rotate(Vector3.forward, thisSpeed);
		if (Right)
			transform.Rotate(Vector3.right, thisSpeed);
	}
}
