﻿using UnityEngine;
using System.Collections;

public class StaticBatcher : MonoBehaviour {

	[SerializeField] GameObject[] _staticObjects;
	[SerializeField] GameObject _root;

	void Start () {
		StaticBatchingUtility.Combine(_staticObjects, _root);
	}
}
