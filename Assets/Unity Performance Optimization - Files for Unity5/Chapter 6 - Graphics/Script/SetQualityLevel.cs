﻿using UnityEngine;
using System.Collections;

public class SetQualityLevel : MonoBehaviour {

	[SerializeField] QualityLevel _qualityLevel;

	void Start () {
		QualitySettings.SetQualityLevel((int)_qualityLevel);
	}
}
