﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfilerDataSaverSetDataPath : MonoBehaviour {

	[SerializeField] Text _textObject;

	void Start() {
		_textObject.text += Application.persistentDataPath;
	}
}
