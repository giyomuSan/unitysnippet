﻿using UnityEngine;
using System.Text;
using System.Collections;

public class ProfilerDataSaverComponent : MonoBehaviour {
	
	int _count = 0;
	
	void Start() {
		UnityEngine.Profiling.Profiler.logFile = "";
	}
	
	void Update () {
		if (Input.GetKey (KeyCode.LeftControl) && Input.GetKeyDown (KeyCode.H)) {
			StopAllCoroutines();
			_count = 0;
			StartCoroutine(SaveProfilerData());
		}
	}
	
	IEnumerator SaveProfilerData() {
		// keep calling this method until Play Mode stops
		while (true) {
			
			// generate the file path
			string filepath = Application.persistentDataPath + "/profilerLog" + _count;
			
			// set the log file and enable the profiler
			UnityEngine.Profiling.Profiler.logFile = filepath;
			UnityEngine.Profiling.Profiler.enableBinaryLog = true;
			UnityEngine.Profiling.Profiler.enabled = true;
			
			// count 300 frames
			for(int i = 0; i < 300; ++i) {
				
				yield return new WaitForEndOfFrame();
				
				// workaround to keep the Profiler working
				if (!UnityEngine.Profiling.Profiler.enabled)
					UnityEngine.Profiling.Profiler.enabled = true;
			}
			
			// start again using the next file name	
			_count++;
		}
	}
}
