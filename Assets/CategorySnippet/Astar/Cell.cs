using UnityEngine;
using UnityEngine.EventSystems;

namespace Astar
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class Cell : MonoBehaviour, IPointerClickHandler
	{
		public Vector2 Position { get { return transform.position; } }
		public bool Weight { get; private set; }

		private static int clicked = 0;
		private static Vector2[] selectedPoints = new Vector2[2];

		public void SetColor(Color32 color)
		{
			GetComponent<SpriteRenderer>().color = color;
		}

		public void SetWeight(bool isWalkable)
		{
			Weight = isWalkable;
			if(!isWalkable) SetColor(new Color32 (0, 0, 0, 255));
		}

		public void OnPointerClick (PointerEventData eventData)
		{
			if (Weight == true && clicked <= 1) 
			{
				GetComponent<SpriteRenderer>().color = new Color32(255, 0, 0, 255);
				selectedPoints [clicked] = Position;

				if(clicked == 1)
				{
					var grid = GameObject.FindObjectOfType(typeof(Grid)) as Grid;
					grid.SolvePath(selectedPoints[0], selectedPoints[1]);
					clicked = 0;
				}
				else
				{
					clicked += 1;
				}
			}
		}
	}
}

