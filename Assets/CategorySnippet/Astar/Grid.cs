﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Astar
{
	public class Grid : MonoBehaviour 
	{
		#region fields
		public int Width;
		public int Height;
		public GameObject Prefab;

		private bool[,] weights;
		private Cell[,] cells;
		#endregion

		#region methods
		void Start()
		{
			weights = new bool[Width, Height];
			cells = new Cell[Width, Height];

			for (int i = 0; i < Width; i++) 
			{
				for(int j = 0; j < Height; j++)
				{
					var go = Instantiate(Prefab, new Vector2(i,j), Quaternion.identity) as GameObject;
					var weight = Random.value > 0.4f ? true : false;

					weights[i,j] = weight;

					// setup quickly cell visual and keep a track of them for later color change
					var cell = go.GetComponent<Cell>();
					cell.SetWeight(weight);
					cells[i,j] = cell;
				}
			}
		}

		public void SolvePath(Vector2 start, Vector2 end)
		{
			var path = Pathfind (start, end);

			foreach (var p in path) 
				cells[(int)p.x, (int)p.y].SetColor(new Color32(0,255,0,255));
		}
		#endregion

		#region pathfind implementation methods
		private List<Vector2> Pathfind(Vector2 start, Vector2 end)
		{
			// nodes that have already been analyzed and have a path from the start to them
			var closedSet = new List<Vector2> ();

			// nodes that have been identified as a neighbor of an analyzed node, but have yet
			// to be fully analyzed
			var openSet = new List<Vector2>{start};

			// a dictionary identifying the optimal origin point to each node.
			// this is used to back-track from the end to find the optimal path
			var cameFrom = new Dictionary<Vector2, Vector2>();

			// a dictionary indicating how far each analyzed node is from the start
			var currentDistance = new Dictionary<Vector2, int>();

			// a dictionary indicating how far it is expected to reach the end,
			// if teh path travel through teh specified node.
			var predictedDistance = new Dictionary<Vector2, float>();

			// initialize the start node as having a distance of 0, and an estimated distance
			// of y-distance + x-distance, which is teh optimal path in a square grid that 
			// doesn't allow for diagonal movement.
			currentDistance.Add (start, 0);
			predictedDistance.Add (start, 0 + Mathf.Abs (start.x - end.x) + Mathf.Abs (start.y - end.y));

			// if there are unanalyzed nodes, process them
			while (openSet.Count > 0) 
			{
				// get the node with the lowest estimated cost to finish
				var current = (from p in openSet orderby predictedDistance[p] ascending select p).First();

				// if it is the finish, return the path by generating the found path
				if(current.x == end.x && current.y == end.y)
					return ReconstructPath(cameFrom, end);

				// move current node from open to closed
				openSet.Remove(current);
				closedSet.Add (current);

				// process each valid node around the current node
				foreach(var neighbor in GetNeighborNodes(current))
				{
					var tempCurrentDistance = currentDistance[current] + 1;

					// if we already know a faster way to this neighbor, use that route and ignore this one
					if(closedSet.Contains(neighbor) && tempCurrentDistance >= currentDistance[neighbor])
						continue;

					// if we don't know a route to this neighbor, or if this is faster , store this route
					if(!closedSet.Contains(neighbor) || tempCurrentDistance < currentDistance[neighbor])
					{
						if(cameFrom.Keys.Contains(neighbor))
							cameFrom[neighbor] = current;
						else
							cameFrom.Add(neighbor, current);
					}

					currentDistance[neighbor] = tempCurrentDistance;
					predictedDistance[neighbor] = currentDistance[neighbor] + Mathf.Abs(neighbor.x - end.x) + Mathf.Abs(neighbor.y - end.y);

					// if this is a new node, add it to processing
					if(!openSet.Contains(neighbor))
						openSet.Add(neighbor);

				}
			}

			// unable to figure out a path, abort
			throw new UnityException (
				string.Format ("unable to find path between {0},{1} and {2},{3}", start.x, start.y, end.x, end.y)
			);
		}

		/// <summary>
		/// Return a list of accessible nodes neighboring a specified node.
		/// </summary>
		/// <returns>a list of nodes neighboring the node that are accessible.</returns>
		/// <param name="node">the center node to be analyzed.</param>
		private IEnumerable<Vector2> GetNeighborNodes(Vector2 node)
		{
			var nodes = new List<Vector2> ();

			// convert to int just to be able to use this as index for weights array
			var x = Mathf.CeilToInt(node.x);
			var y = Mathf.CeilToInt(node.y);

			// clockwise check (up, right, down, left) order
			if (y + 1 < Height && weights [x, y + 1] == true) 
				nodes.Add (new Vector2 (x, y + 1));

			if (x + 1 < Width && weights [x + 1, y] == true) 
				nodes.Add (new Vector2 (x + 1, y));

			if (y - 1 >= 0 && weights [x, y - 1] == true)
				nodes.Add (new Vector2 (x, y - 1));

			if (x - 1 >= 0 && weights [x - 1, y] == true)
				nodes.Add (new Vector2 (x - 1, y));

			return nodes;
		}

		/// <summary>
		/// Process a list of valid paths generated by the pathfind function and return
		/// a coherent path to current.
		/// </summary>
		/// <returns>The shortest path from the start to destination</returns>
		/// <param name="cameFrom">A list of node and the origin to that node</param>
		/// <param name="current">The destination node being sought out</param>
		private List<Vector2> ReconstructPath(Dictionary<Vector2, Vector2> cameFrom, Vector2 current)
		{
			if(!cameFrom.Keys.Contains(current))
			   return new List<Vector2>{current};

			var path = ReconstructPath (cameFrom, cameFrom [current]);
			path.Add (current);
			return path;
		}
		#endregion
	}
}
