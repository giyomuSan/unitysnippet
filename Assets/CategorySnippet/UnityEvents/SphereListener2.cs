﻿using UnityEngine;

public class SphereListener2 : MonoBehaviour
{
    void Start()
    {
        FindObjectOfType<UnityEventTest>().ColorEvent.AddListener(ChangeMaterialAlbedoColor);
    }

    void ChangeMaterialAlbedoColor(Color newColor)
    {
        var mat = GetComponent<Renderer>().material;
        if(mat)
        {
            mat.SetColor("_Color", newColor);
        }
    }
}