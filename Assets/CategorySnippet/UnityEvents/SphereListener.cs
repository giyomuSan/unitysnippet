﻿using UnityEngine;

public class SphereListener : MonoBehaviour
{
    void Start()
    {
        FindObjectOfType<UnityEventTest>().MyEvent.AddListener(AddVerticalForce);
    }

    void AddVerticalForce()
    {
        var rb = GetComponent<Rigidbody>();
        if(rb)
        {
            var cameraDirectionVector = Camera.main.transform.forward;
            var mul = Random.value * 20f;

            rb.AddForce(cameraDirectionVector * mul, ForceMode.Impulse);
        }
    }
}
