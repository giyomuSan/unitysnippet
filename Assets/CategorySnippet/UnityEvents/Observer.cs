using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class Observer
{
	#region singleton
	private static Observer instance;
	public static Observer Instance
	{
		get 
		{
			if (instance == null)
			{
				instance = new Observer();
			}

			return instance;
		}
	}

	private Observer()
	{
		eList = new Dictionary<string, UnityEvent> ();
	}
	#endregion

	#region fields
	private Dictionary<string, UnityEvent> eList;
	#endregion

	#region methods
	public void Add(string eventName, UnityAction listener)
	{
		UnityEvent e;

		// check if this event exist in the list first
		if (!eList.TryGetValue (eventName, out e)) 
		{
			e = new UnityEvent ();
			e.AddListener (listener);
			eList.Add (eventName, e);
		} 
		else
		{
			e.AddListener(listener);
		}
	}

	public void Remove(string eventName, UnityAction listener)
	{
		UnityEvent e;

		if(eList.TryGetValue(eventName, out e))
		   e.RemoveListener(listener);
	}

	public void Send(string eventName, object sender = null)
	{
		UnityEvent e;

		if (eList.TryGetValue (eventName, out e)) 
			e.Invoke ();
	}
	#endregion
}

