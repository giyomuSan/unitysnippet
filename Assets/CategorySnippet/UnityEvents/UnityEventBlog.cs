﻿using UnityEngine;
using UnityEngine.Events;

public class UnityEventBlog : MonoBehaviour
{
	private const string onSomeEvent = "UnityEventBlogOnSomeEvent";
	private UnityAction someListener;

	void Start()
	{
		// create our action delegate
		someListener = new UnityAction (SomeEventCallback);

		// create or add our event
		Observer.Instance.Add (onSomeEvent, someListener);
	}
	
    void Update()
    {
        if (Input.anyKeyDown) 
		{
			// trigger our event from our Observer class
			Observer.Instance.Send(onSomeEvent, this);
		}
    }

	void SomeEventCallback()
	{
		gameObject.name = "GameObject name has changed .." + Random.Range(1,100).ToString();

		// Remove that event from our Observer 
		Observer.Instance.Remove (onSomeEvent, someListener);
	}
}


