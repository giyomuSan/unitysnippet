﻿using UnityEngine;
using UnityEngine.Events;

public class UnityEventTest : MonoBehaviour
{
    public UnityEvent MyEvent;
    public MyColorEvent ColorEvent;

    void Awake()
    {
        if (MyEvent == null)
            MyEvent = new UnityEvent();

        if (ColorEvent == null)
            ColorEvent = new MyColorEvent();
    }

    void Update()
    {
        if(Input.anyKeyDown)
        {
            if(MyEvent != null)
                MyEvent.Invoke();

            if(ColorEvent != null)
                ColorEvent.Invoke(GetRandomColor());
           
        }
    }

    Color GetRandomColor()
    {
        var r = Random.value;
        var g = Random.value;
        var b = Random.value;

        return new Color(r, g, b, 1f);
    }
}

public class MyColorEvent : UnityEvent<Color> { }

