﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIRawImageScrollTest : MonoBehaviour
{
    [Range(-0.5f, 0.5f)]
    public float ScrollinSpeed;

    void OnEnable()
    {
        StartCoroutine(Scroll());
    }

    IEnumerator Scroll()
    {
        var imageToScroll = GetComponent<RawImage>();
        if (imageToScroll)
        {
            var uvRect = imageToScroll.uvRect;
            while (enabled)
            {
                uvRect.x += ScrollinSpeed * Time.deltaTime;
                imageToScroll.uvRect = uvRect;

                yield return null;
            }
        }
    }
}
