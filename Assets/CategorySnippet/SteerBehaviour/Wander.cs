﻿using System;
using UnityEngine;

namespace SteerBehaviour
{
    public class Wander : SteeringBehaviour
    {
        #region fields
        public float CenterOffset = 10f;
        public float Radius = 3f;
        public float LookAheadTime = 5f;
        public bool IsDebugActive = true;

        private Vector3 targetPosition;
        private float turnAccelerationSqr;

        private const float rotationMatch = 0.0001f;
        #endregion

        #region methods
        void Awake()
        {
            steeringID = SteeringID.Wander;
        }

        protected override void Start()
        {
            base.Start();
            turnAccelerationSqr = LookAheadTime * LookAheadTime;
        }

        public override Vector3 GetSteering()
        {
            if (!CanUpdateFrame()) return steering;

            CalculateSteering();
            return steering * weight;
        }

        protected override void CalculateSteering()
        {
            // define our circle location
						Vector3 projectedPoint = cachedTransform.position + cachedTransform.rotation * new Vector3(0f,0f,CenterOffset);

						// projected point could be use directly , I keep separate for debug purpose
						targetPosition = projectedPoint;
						targetPosition += UnityEngine.Random.onUnitSphere * Radius;

            // delegate to a face behaviour like
						steering = DelegateToFace() / LookAheadTime;
        }

        Vector3 DelegateToFace()
        {
            var direction = targetPosition - cachedTransform.position;

            if (direction.sqrMagnitude < rotationMatch) return Vector3.zero;

            // check our acceleration is not to fast
            if(direction.sqrMagnitude > turnAccelerationSqr)
            {
                Vector3.Normalize(direction);
                direction *= LookAheadTime;
            }

						return direction;
        }

        // not implemented for wander
        public override void SetNewTarget(Transform newTarget) { }

				void OnDrawGizmos()
				{
					// Visualize our Sphere and Target
					if (cachedTransform != null && IsDebugActive) 
					{
						Gizmos.color = Color.gray;
						Vector3 projectedPoint = cachedTransform.position + cachedTransform.rotation * new Vector3(0f,0f,CenterOffset);
						Gizmos.DrawWireSphere(projectedPoint, Radius);
						Gizmos.DrawLine(cachedTransform.position, projectedPoint);

						Gizmos.color = Color.red;
						Gizmos.DrawSphere(targetPosition, 0.3f);

						Gizmos.color = Color.red;
						Gizmos.DrawLine(cachedTransform.position, targetPosition);
					}
				}
        #endregion
    }
}
