﻿using UnityEngine;

namespace SteerBehaviour
{
    public class Constant : SteeringBehaviour
    {
        #region fields
        public Vector3 Direction = Vector3.forward;
        #endregion

        #region methods
        void Awake()
        {
            steeringID = SteeringID.Constant;
        }

        protected override void Start()
        {
            base.Start();
            CalculateSteering();
        }

        public override Vector3 GetSteering()
        {
            if (!CanUpdateFrame()) return steering;

            CalculateSteering();
            return steering * weight;
        }

        // not implemented for constant
        public override void SetNewTarget(Transform newTarget) { }

        protected override void CalculateSteering()
        {
            steering = Direction.normalized;
        }
        #endregion
    }
}
