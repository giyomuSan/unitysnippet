﻿using UnityEngine;
using System.Collections;

namespace SteerBehaviour
{
    [RequireComponent(typeof(Rigidbody))]
    public class Entity : MonoBehaviour
    {
        #region fields
        public float Mass = 1f;
        public float MaxSpeed = 1f;
        public float Force = 0f;
        public float SmoothheadingFactor = 7f;

        private Vector3 acceleration = Vector3.zero;
        private Vector3 newVelocity = Vector3.zero;
        private Quaternion newRotation = Quaternion.identity;
        private Rigidbody cachedRigibody;
        private bool isActive;

        [SerializeField]
        private bool isElevationFrozen = true;

        [SerializeField]
        private bool isHeadingFollowVelocity = true;

        [SerializeField]
        private bool isHeadingKeptVertical = true;

        private SteeringBehaviour[] storedBehaviours;

        private const float minSqrVelocity = 0.000001f;
        #endregion

        #region properties
        public Vector3 SteeringVector { get; set; }
        public Vector3 HeadingVector { get; set; }

        public bool IsEntityActive
        {
            get
            {
                return isActive;
            }

            set
            {
                isActive = value;
                if (isActive)
                    StartCoroutine(UpdateSteering());
            }
        }
        #endregion

        #region methods
        void Start()
        {
            // Make sure we have steering component to work with
            storedBehaviours = GetComponents<SteeringBehaviour>();
            if(storedBehaviours.Length == 0 || storedBehaviours == null)
            {
                Debug.LogWarning("No SteeringBehaviour detected on : " + gameObject.name);
                gameObject.SetActive(false);
            }

			// rigidbody
			cachedRigibody = GetComponent<Rigidbody>();
			if (isElevationFrozen)
				cachedRigibody.useGravity = false;

            // initialize steer and heading with initial vector
            SteeringVector = Vector3.forward;
            HeadingVector = Vector3.forward;

            // update sttering by default
            IsEntityActive = true;
        }

		// not implemented
		public void EnableBehaviours(SteeringID id, bool state){}

        IEnumerator UpdateSteering()
        {
            while (isActive)
            {
                SteeringAccumulator();
                if (!isHeadingFollowVelocity) HeadingAccumulator();

                Move();
                Orient();

                DebugVelocities();

                yield return null;
            }
        }

        void Move()
        {
            // check for non zero steering vector
            if(SteeringVector == Vector3.zero)
            {
                cachedRigibody.Sleep();
                return;
            }

            acceleration = SteeringVector / Mass;
            newVelocity += acceleration * (Time.deltaTime + Force);
            newVelocity = Vector3.ClampMagnitude(newVelocity, MaxSpeed);

            if (isElevationFrozen) newVelocity.y = 0f;

            cachedRigibody.velocity = newVelocity;
        }

        void Orient()
        {
            // check for non zero heading vector
            if (HeadingVector == Vector3.zero) return;

            if (isHeadingFollowVelocity && newVelocity != Vector3.zero)
                HeadingVector = newVelocity;

            if (HeadingVector.sqrMagnitude > minSqrVelocity)
                Vector3.Normalize(HeadingVector);

            if (isHeadingKeptVertical)
                HeadingVector = new Vector3(HeadingVector.x, 0f, HeadingVector.z);

            newRotation = Quaternion.Slerp(cachedRigibody.rotation, Quaternion.LookRotation(HeadingVector), SmoothheadingFactor * Time.deltaTime);
            cachedRigibody.rotation = newRotation;

        }

        void SteeringAccumulator()
        {
            SteeringVector = Vector3.zero;
            bool isForSteer;

            for(int i = 0; i < storedBehaviours.Length; i++)
            {
                if (!storedBehaviours[i].enabled) continue;

                isForSteer = storedBehaviours[i].GetSteerUpdateType == SteeringBehaviour.UpdateSteerFor.Steering | storedBehaviours[i].GetSteerUpdateType == SteeringBehaviour.UpdateSteerFor.Both;
                if (isForSteer) SteeringVector += storedBehaviours[i].GetSteering();
            }

            Vector3.Normalize(SteeringVector);
        }

        void HeadingAccumulator()
        {
            HeadingVector = Vector3.zero;
            bool isForHeading;

            for (int i = 0; i < storedBehaviours.Length; i++)
            {
                if (!storedBehaviours[i].enabled) continue;

                isForHeading = storedBehaviours[i].GetSteerUpdateType == SteeringBehaviour.UpdateSteerFor.Heading | storedBehaviours[i].GetSteerUpdateType == SteeringBehaviour.UpdateSteerFor.Both;
                if (isForHeading) HeadingVector += storedBehaviours[i].GetSteering();
            }
        }

        void DebugVelocities()
        {
            // steering
            Debug.DrawRay(cachedRigibody.position, cachedRigibody.velocity, Color.red);

            // heading
            Debug.DrawRay(cachedRigibody.position, HeadingVector, Color.blue);
        }
        #endregion
    }
}
