﻿using UnityEngine;

namespace SteerBehaviour
{
    [RequireComponent(typeof(Entity))] 
    public abstract class SteeringBehaviour : MonoBehaviour
    {
        #region enum
        public enum UpdateSteerFor
        {
            Steering,
            Heading,
            Both
        }
        #endregion

        #region fields
        protected SteeringID steeringID;
        protected Rigidbody cachedRigibody;
        protected Transform cachedTransform;
        protected Vector3 steering;
        [SerializeField]
        protected UpdateSteerFor updateSteerFor;
        [SerializeField]
        [Range(0f,1f)]
        protected float weight = 1f;
        [SerializeField]
        protected float updateRateInSeconds;

        private float elapsedTime = 0f;
        #endregion

        #region properties
        public SteeringID GetSteeringId { get { return steeringID; } }
        public UpdateSteerFor GetSteerUpdateType { get { return updateSteerFor; } }
        #endregion

        #region methods
        public abstract Vector3 GetSteering();
        public abstract void SetNewTarget(Transform newTarget);
        protected abstract void CalculateSteering();

        protected virtual void Start()
        {
            GetRequiredComponent();
        }

        protected void GetRequiredComponent()
        {
            cachedRigibody = GetComponent<Rigidbody>();
            cachedTransform = GetComponent<Transform>();
        }

        protected bool CanUpdateFrame()
        {
            // always return true if our updateRateInSeconds = 0 or less
            if (updateRateInSeconds <= 0f) return true;

            elapsedTime += Time.deltaTime;
			if (elapsedTime < updateRateInSeconds) return false;
            else
            {
                elapsedTime = 0;
                return true;
            }
        }
        #endregion
    }

    public enum SteeringID
    {
        Unkonwn,
        Wander,
        Constant,
        Seek,
        Flee
    }
}
