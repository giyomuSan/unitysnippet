﻿using UnityEngine;

namespace SteerBehaviour
{
    public class Flee : SteeringBehaviour
    {
        #region fields
        public Transform Target;
        [Range(0f, 1f)]
        public float MaxAcceleration = 1f;
        public bool IsDebugActive = true;
        #endregion

        #region methods
        void Awake()
        {
            steeringID = SteeringID.Flee;
        }

        protected override void Start()
        {
            base.Start();
            CalculateSteering();
        }

        public override Vector3 GetSteering()
        {
            if (!CanUpdateFrame()) return steering;

            CalculateSteering();
            return steering * weight;
        }

        public override void SetNewTarget(Transform newTarget)
        {
            Target = newTarget;
        }

        protected override void CalculateSteering()
        {
            if (Target == null || cachedTransform == null)
            {
                steering = Vector3.zero;
                return;
            }

            steering = (cachedTransform.position - Target.position).normalized;
            steering *= MaxAcceleration;
        }

        void OnDrawGizmos()
        {
            if (IsDebugActive && steering != Vector3.zero)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(cachedTransform.position, Target.position);
            }
        }
        #endregion
    }
}
