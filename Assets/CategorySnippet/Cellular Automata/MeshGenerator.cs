using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshGenerator : MonoBehaviour
{
    #region fields
    public SquareGrid Grid;
    public bool ShowDebug = true;

    private List<Vector3> vertices;
    private List<int> triangles;
    #endregion

    #region methods
    public void GenerateMesh(int[,] map, float squareSize)
    {
        Grid = new SquareGrid(map, squareSize);

        vertices = new List<Vector3>();
        triangles = new List<int>();

        for (int x = 0; x < Grid.Squares.GetLength(0); x++)
        {
            for (int y = 0; y < Grid.Squares.GetLength(1); y++)
            {
                TriangulateSquare(Grid.Squares[x,y]);
            }
        }

        // create our mesh
        Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
    }

    void TriangulateSquare(Square square)
    {
        switch (square.Configuration)
        {
            case 0:
                break;

            // 1 point configuration value
            case 1:
                MeshFromPoints(square.CenterBottom, square.BottomLeft, square.CenterLeft);
                break;
            case 2:
                MeshFromPoints(square.CenterRight, square.BottomRight, square.CenterBottom);
                break;
            case 4:
                MeshFromPoints(square.CenterTop, square.TopRight, square.CenterRight);
                break;
            case 8:
                MeshFromPoints(square.TopLeft, square.CenterTop, square.CenterLeft);
                break;

           // 2 points configuration value
            case 3:
                MeshFromPoints(square.CenterRight, square.BottomRight, square.BottomLeft, square.CenterLeft);
                break;
            case 6:
                MeshFromPoints(square.CenterTop, square.TopRight, square.BottomRight, square.CenterBottom);
                break;
            case 9:
                MeshFromPoints(square.TopLeft, square.CenterTop, square.CenterBottom, square.BottomLeft);
                break;
            case 12:
                MeshFromPoints(square.TopLeft, square.TopRight, square.CenterRight, square.CenterLeft);
                break;
            case 5:
                MeshFromPoints(square.CenterTop, square.TopRight, square.CenterRight, square.CenterBottom, square.BottomLeft, square.CenterLeft);
                break;
            case 10:
                MeshFromPoints(square.TopLeft, square.CenterTop, square.CenterRight, square.BottomRight, square.CenterBottom, square.CenterLeft);
                break;

            // 3 points configuration value
            case 7:
                MeshFromPoints(square.CenterTop, square.TopRight, square.BottomRight, square.BottomLeft, square.CenterLeft);
                break;
            case 11:
                MeshFromPoints(square.TopLeft, square.CenterTop, square.CenterRight, square.BottomRight, square.BottomLeft);
                break;
            case 13:
                MeshFromPoints(square.TopLeft, square.TopRight, square.CenterRight, square.CenterBottom, square.BottomLeft);
                break;
            case 14:
                MeshFromPoints(square.TopLeft, square.TopRight, square.BottomRight, square.CenterBottom, square.CenterLeft);
                break;

            // 4 points configuration value
            case 15:
                MeshFromPoints(square.TopLeft, square.TopRight, square.BottomRight, square.BottomLeft);
                break;
        }
    }

    void MeshFromPoints(params Node[] points)
    {
        AssignVertices(points);

        if(points.Length >= 3) CreateTriangle(points [0], points [1], points [2]);
        if(points.Length >= 4) CreateTriangle(points [0], points [2], points [3]);
        if(points.Length >= 5) CreateTriangle(points [0], points [3], points [4]);
        if(points.Length >= 6) CreateTriangle(points [0], points [4], points [5]);
    }

    void AssignVertices(Node[] points)
    {
        for (int i = 0; i < points.Length; i++)
        {
            if(points[i].VertexIndex == -1)
            {
                points[i].VertexIndex = vertices.Count;
                vertices.Add(points[i].Position);
            }
        }
    }

    void CreateTriangle(Node a, Node b, Node c)
    {
        triangles.Add(a.VertexIndex);
        triangles.Add(b.VertexIndex);
        triangles.Add(c.VertexIndex);
    }

    // Draw our control node and node in the scene for check purpose.
    void OnDrawGizmos()
    {
        if (Grid != null && ShowDebug)
        {
            float controlNodeSize = 0.4f;
            float nodeSize = 0.15f;
            for(int x = 0; x < Grid.Squares.GetLength(0); x++)
            {
                for(int y = 0; y < Grid.Squares.GetLength(1); y++)
                {
                    // TopLeft
                    Gizmos.color = (Grid.Squares[x,y].TopLeft.IsActive)? Color.black: Color.white;
                    Gizmos.DrawCube(Grid.Squares[x,y].TopLeft.Position, Vector3.one * controlNodeSize);

                    // TopRight
                    Gizmos.color = (Grid.Squares[x,y].TopRight.IsActive)? Color.black: Color.white;
                    Gizmos.DrawCube(Grid.Squares[x,y].TopRight.Position, Vector3.one * controlNodeSize);

                    // BottomRight
                    Gizmos.color = (Grid.Squares[x,y].BottomRight.IsActive)? Color.black: Color.white;
                    Gizmos.DrawCube(Grid.Squares[x,y].BottomRight.Position, Vector3.one * controlNodeSize);

                    // BottomLeft
                    Gizmos.color = (Grid.Squares[x,y].BottomLeft.IsActive)? Color.black: Color.white;
                    Gizmos.DrawCube(Grid.Squares[x,y].BottomLeft.Position, Vector3.one * controlNodeSize);

                    Gizmos.color = Color.grey;
                    Gizmos.DrawCube(Grid.Squares[x,y].CenterTop.Position, Vector3.one * nodeSize);
                    Gizmos.DrawCube(Grid.Squares[x,y].CenterRight.Position, Vector3.one * nodeSize);
                    Gizmos.DrawCube(Grid.Squares[x,y].CenterBottom.Position, Vector3.one * nodeSize);
                    Gizmos.DrawCube(Grid.Squares[x,y].CenterLeft.Position, Vector3.one * nodeSize);
                }
            }
        }
    }
    #endregion

    #region classes
    public class SquareGrid
    {
        public Square[,] Squares;

        public SquareGrid(int[,] map, float squareSize)
        {
            int nodeCountInX = map.GetLength(0);
            int nodeCountInY = map.GetLength(1);

            float mapWidth = nodeCountInX * squareSize;
            float mapHeight = nodeCountInY * squareSize;

            ControlNode[,] controlNodes = new ControlNode[nodeCountInX,nodeCountInY];

            for(int x = 0; x < nodeCountInX; x++)
            {
                for(int y = 0; y < nodeCountInY; y++)
                {
                    // calculate position of our current control node
                    Vector3 position = new Vector3(-mapWidth/2 + x * squareSize + squareSize/2, 0, -mapHeight/2 + y * squareSize + squareSize/2);

                    // set our control nodes
                    controlNodes[x,y] = new ControlNode(position, map[x,y] == 1, squareSize);
                }
            }

            // create a grid of squares
            Squares = new Square[nodeCountInX - 1, nodeCountInY - 1];
            for(int x = 0; x < nodeCountInX - 1; x++)
            {
                for(int y = 0; y < nodeCountInY - 1; y++)
                {
                    // set our square
                    Squares[x,y] = new Square(controlNodes[x,y+1], controlNodes[x+1,y+1], controlNodes[x+1,y], controlNodes[x,y]);
                }
            }
        }
    }

    public class Square
    {
        public ControlNode TopLeft, TopRight, BottomRight, BottomLeft;
        public Node CenterTop, CenterRight, CenterBottom, CenterLeft;
        public int Configuration;

        public Square(ControlNode topLeft, ControlNode topRight, ControlNode bottomRight, ControlNode bottomLeft)
        {
            // Set control node
            TopLeft = topLeft;
            TopRight = topRight;
            BottomRight = bottomRight;
            BottomLeft = bottomLeft;

            // set mid point node
            CenterTop = TopLeft.Right;
            CenterRight = BottomRight.Above;
            CenterBottom = BottomLeft.Right;
            CenterLeft = BottomLeft.Above;

            // Set configurartion value
            if(TopLeft.IsActive) Configuration += 8;
            if(TopRight.IsActive) Configuration += 4;
            if(BottomRight.IsActive) Configuration += 2;
            if(BottomLeft.IsActive) Configuration += 1;
        }
    }

    public class Node
    {
        public Vector3 Position;
        public int VertexIndex = -1;

        public Node(Vector3 position)
        {
            Position = position;
        }
    }

    public class ControlNode : Node
    {
        public bool IsActive;
        public Node Above, Right;

        public ControlNode(Vector3 position, bool isActive, float squareSize) : base(position)
        {
            IsActive = isActive;
            Above = new Node(position + Vector3.forward * squareSize/2f);
            Right = new Node(position + Vector3.right * squareSize/2f);
        }
    }
    #endregion
}

