﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour
{
    #region fields
    public int Width;
    public int Height;
    public string Seed;
    public bool UseRandomSeed;
    [Range(0,100)]
    public int RandomFillPercent;
    [Range(1,10)]
    public int SmoothingStepCount;
    public bool ShowDebug = true;
    private int[,] map;
    #endregion

    #region Unity callback
    void Start()
    {
        GenerateMap();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            GenerateMap();
    }
    #endregion

    #region map generation methods
    void GenerateMap()
    {
        map = new int[Width, Height];
        RandomFillMap();

        for(int i = 0; i < SmoothingStepCount; i++)
            SmoothMap();

        // calling our method to generate our mesh
        // make sure this component is on the same gameObject of that script
        MeshGenerator meshGen = GetComponent<MeshGenerator>();
        meshGen.GenerateMesh(map, 1);
    }

    void RandomFillMap()
    {
        if (UseRandomSeed)
            Seed = Time.time.ToString();

        System.Random pseudoRandom = new System.Random(Seed.GetHashCode());

        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                // make sure border of the map are filled with 1
                if(x== 0 || x == Width - 1 || y == 0 || y == Height - 1)
                    map[x,y] = 1;
                else
                    map [x, y] = (pseudoRandom.Next(0, 100) < RandomFillPercent) ? 1 : 0;
            }
        }
    }

    void SmoothMap()
    {
        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                int neighborWallTiles = GetSurroundingWallCount(x,y);
                if(neighborWallTiles > 4)
                    map[x,y] = 1;
                else if(neighborWallTiles < 4)
                    map[x,y] = 0;
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighborX = gridX - 1; neighborX <= gridX + 1; neighborX++)
        {
            for (int neighborY = gridY - 1; neighborY <= gridY + 1; neighborY++)
            {
                if(neighborX >= 0 && neighborX < Width && neighborY >= 0 && neighborY < Height)
                {
                    if(neighborX != gridX || neighborY != gridY)
                        wallCount += map[neighborX, neighborY];
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }
    #endregion

    #region Unity debug methods
    void OnDrawGizmos()
    {
        if (map != null && ShowDebug)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Gizmos.color = (map [x, y] == 1) ? Color.black : Color.white;
                    Vector3 pos = new Vector3(-Width/2 + x + 0.5f, 0, -Height/2 + y + 0.5f);
                    Gizmos.DrawCube(pos, Vector3.one);
                }
            }
        }
    }
    #endregion
}
