using UnityEngine;

public class CameraClipPointUsage : MonoBehaviour
{
	public Transform Target;
	
	void OnDrawGizmos()
	{
		if(Target != null)
		{
			Gizmos.color = Color.red;
			var cp = CameraHelper.ClipPlaneAtNear(transform.position);
			var tp = Target.position;

			// four camera clip point
			Gizmos.DrawLine(cp.UpperLeft, tp);
			Gizmos.DrawLine(cp.LowerRight, tp);
			Gizmos.DrawLine(cp.UpperRight, tp);
			Gizmos.DrawLine(cp.LowerLeft, tp);

			// last line for camera center check and position target ( to prevent the small space between camera pos and near clip plane )
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(transform.position, tp);
		}
	}
}

