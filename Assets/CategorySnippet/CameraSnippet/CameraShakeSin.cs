﻿using UnityEngine;
using System;
using System.Collections;

namespace CameraUtility
{
    public class CameraShakeSin : MonoBehaviour
    {
        #region fields
        [SerializeField] private float startDistance = 0.8f;
        [SerializeField] private float decreaseRate = 0.5f;
        [SerializeField] private float speed = 55f;
        [SerializeField] private int cycleNumber = 3;
        [SerializeField] private bool xShake, yShake, zShake;

        private int xMul, yMul, zMul;
        #endregion

        #region methods
        void OnEnable()
        {
            #if UNITY_EDITOR
            var axes = new BoolVector3(xShake, yShake, zShake);
            StartCoroutine(Shake(startDistance, decreaseRate, speed, cycleNumber, axes, () => enabled = false));
            #endif
        }

        public IEnumerator Shake(float startDistance, float decrease, float speed, float cycleNumber, BoolVector3 shakeAxes, Action onFinish)
        {
            SetAxis(shakeAxes);

            var thisTransform = transform;
            var initialPosition = thisTransform.position;
            var hitTime = Time.time;
            var timer = 0f;
            var motion = 0f;

            while(cycleNumber > 0)
            {
                timer = (Time.time - hitTime) * speed;
                motion = Mathf.Sin(timer) * startDistance;

                thisTransform.position = new Vector3(initialPosition.x + motion * xMul, initialPosition.y + motion * yMul, initialPosition.z + motion * zMul);

                if(timer > Mathf.PI * 2)
                {
                    hitTime = Time.time;
                    startDistance *= decreaseRate;
                    cycleNumber--;
                }

                yield return null;
            }

            if (onFinish != null) onFinish();
        }

        void SetAxis(BoolVector3 axes)
        {
            xMul = axes.X ? 1 : 0;
            yMul = axes.Y ? 1 : 0;
            zMul = axes.Z ? 1 : 0;
        }
        #endregion
    }

    public struct BoolVector3
    {
        public bool X;
        public bool Y;
        public bool Z;

        public BoolVector3(bool x, bool y, bool z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
