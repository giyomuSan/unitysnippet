﻿using UnityEngine;

public struct ClipPlanePoints
{
	public Vector3 UpperLeft;
	public Vector3 UpperRight;
	public Vector3 LowerLeft;
	public Vector3 LowerRight;
}

public static class CameraHelper
{
    public static ClipPlanePoints ClipPlaneAtNear(Vector3 position)
    {
        ClipPlanePoints clipPlanePoints = new ClipPlanePoints();

        if (!Camera.main) return clipPlanePoints;
		
				var camTransform = Camera.main.transform;

        // set our value needed to calculate points later
        var halfFOV = (Camera.main.fieldOfView * 0.5f) * Mathf.Deg2Rad;
        var aspect = Camera.main.aspect;
        var distance = Camera.main.nearClipPlane;
        var height = distance * Mathf.Tan(halfFOV);
        var width = height * aspect;

        // calculate all point in world space
				clipPlanePoints.LowerLeft = position - camTransform.right * width;
				clipPlanePoints.LowerLeft -= camTransform.up * height;
				clipPlanePoints.LowerLeft += camTransform.forward * distance;

				clipPlanePoints.LowerRight = position + camTransform.right * width;
				clipPlanePoints.LowerRight -= camTransform.up * height;
				clipPlanePoints.LowerRight += camTransform.forward * distance;

				clipPlanePoints.UpperLeft = position - camTransform.right * width;
				clipPlanePoints.UpperLeft += camTransform.up * height;
				clipPlanePoints.UpperLeft += camTransform.forward * distance;

				clipPlanePoints.UpperRight = position + camTransform.right * width;
				clipPlanePoints.UpperRight += camTransform.up * height;
				clipPlanePoints.UpperRight += camTransform.forward * distance;
		
        return clipPlanePoints;
    }
}
