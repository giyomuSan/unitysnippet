﻿using UnityEngine;

public class SimpleCharacterMove : MonoBehaviour
{
    public float Acceleration = 5f;
    public float JumpForce = 20f;
    private Rigidbody rb;
    Vector3 velocity;

	void Start ()
    {
        rb = GetComponent<Rigidbody>();
	}

    void Update()
    {
        float hor = Input.GetAxisRaw("Horizontal");

        if (hor > 0) 
        {
            transform.eulerAngles = new Vector3(0, 90f, 0);
            velocity.x = (hor * Time.deltaTime) + Acceleration;
        }
        else if (hor < 0) 
        {
            transform.eulerAngles = new Vector3(0, -90f, 0);
            velocity.x = (hor * Time.deltaTime) - Acceleration;
        }
        else velocity = Vector3.zero;

        if (Input.GetKeyDown(KeyCode.UpArrow))
            rb.AddForce(new Vector3(0f, JumpForce, 0f), ForceMode.Impulse);

        velocity.y = rb.velocity.y;
        rb.velocity = velocity;
    }
}
