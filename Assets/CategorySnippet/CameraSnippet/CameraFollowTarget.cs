﻿using UnityEngine;

namespace CameraSnippet
{
    public class CameraFollowTarget : MonoBehaviour
    {
        #region fields
        [SerializeField] private Transform target;
        [SerializeField] private float distanceToTarget = 3f;
        [SerializeField] private float heightToTarget = 0f;
        [SerializeField] private float horizontalOffset = 0f;
        [SerializeField] private float forwardDamping = 0.5f;
        [SerializeField] private float horizontalDamping = 0.5f;
        [SerializeField] private float verticalDamping = 0.5f;
        [SerializeField] private bool xLock, yLock, zLock;

        private Transform thisTransform;
        private Vector3 velocityDamping;
        #endregion

        #region methods
        void Start()
        {
            thisTransform = transform;

            // set camera to have a side view
            thisTransform.eulerAngles = Vector3.zero;
        }

        void LateUpdate()
        {
            // this is set for a XY plane, switch x and z for a XZ plane
            if (target)
            {
                var newPosition = thisTransform.position;

                // adjust horizontal offset from target direction based on Y euler angles
                var offset = IsSameDirection() ? -horizontalOffset : horizontalOffset;

                // positioning
                if(!xLock) newPosition.x = Mathf.SmoothDamp(thisTransform.position.x, target.position.x + offset, ref velocityDamping.x, horizontalDamping);
                if(!yLock) newPosition.y = Mathf.SmoothDamp(thisTransform.position.y, target.position.y + heightToTarget, ref velocityDamping.y, verticalDamping);

                // zoom
                if(!zLock) newPosition.z = Mathf.SmoothDamp(thisTransform.position.z, target.position.z - distanceToTarget, ref velocityDamping.z, forwardDamping);

                // update our position
                thisTransform.position = newPosition; 
            }

        }

        bool IsSameDirection()
        {
            return (target.forward - thisTransform.right).normalized.x != 0f;
        }

        void OnCameraZoom(float distanceToTarget, float heightToTarget, float forwardDamping)
        {
            this.forwardDamping = forwardDamping;
            this.distanceToTarget = distanceToTarget;
            this.heightToTarget = heightToTarget;
        }

        void OnCameraAxisLock(bool x, bool y, bool z)
        {
            xLock = x;
            yLock = y;
            zLock = z;
        }

        void OnCameraTargetChange(Transform target)
        {
            this.target = target;
        }

        void OnCameraFocusZone(float horizontalOffset, float heightToTarget, float distanceToTarget)
        {
            this.horizontalOffset = horizontalOffset;
            this.heightToTarget = heightToTarget;
            this.distanceToTarget = distanceToTarget;
        }

        #endregion
    }
}
