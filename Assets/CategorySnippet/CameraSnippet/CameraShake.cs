﻿using UnityEngine;
using System;
using System.Collections;

namespace CameraUtility
{
    public class CameraShake : MonoBehaviour
    {
        #region fields
        [SerializeField] private float duration = 1f;
        [SerializeField] private float magnitude = 0.1f;
        #endregion

        #region methods
        void OnEnable()
        {
            #if UNITY_EDITOR
            // Just for quick test purpose
            StartCoroutine(Shake(duration, magnitude, () => enabled = false));
            #endif
        }

        public IEnumerator Shake(float duration, float magnitude, Action onFinish)
        {
            float elapsed = 0f;
            Transform cameraTransform = Camera.main.transform;
            Vector3 cameraOriginPos = cameraTransform.position;

            while (elapsed < duration)
            {
                elapsed += Time.deltaTime;

                float percentComplete = elapsed / duration;
                float damper = 1f - Mathf.Clamp(4f * percentComplete - 3f, 0f, 1f);

                // map from -1 to 1
                float x = UnityEngine.Random.value * 2f - 1f;
                float y = UnityEngine.Random.value * 2f - 1f;

                x *= magnitude * damper;
                y *= magnitude * damper;

                cameraTransform.position = new Vector3(x + cameraOriginPos.x, y + cameraOriginPos.y, cameraOriginPos.z);
                yield return null;
            }

            cameraTransform.position = cameraOriginPos;
            if (onFinish != null) onFinish();
        }
        #endregion
    }
}
