﻿using UnityEngine;
using System.Collections;

namespace ProfilerExample
{
    public class ProfilerDataSaverComponent : MonoBehaviour
    {
        private int count;
        
        void Start()
        {
            UnityEngine.Profiling.Profiler.logFile = "";
        }
        
        void Update()
        {
            if(Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.H))
            {
                StopAllCoroutines();
                count = 0;
                StartCoroutine(SaveProfilerData());
            }
        }
        
        IEnumerator SaveProfilerData()
        {
            string filepath = "";
            
            // Keep calling this until Play mode stops
            while(true)
            {
                // generate filepath
                filepath = Application.persistentDataPath + "/profilerLog" + count;
                
                // set log file and enabled profiler;
                UnityEngine.Profiling.Profiler.logFile = filepath;
                UnityEngine.Profiling.Profiler.enableBinaryLog = true;
                UnityEngine.Profiling.Profiler.enabled = true;
                
                // count 300 frame
                for(int i = 0; i < 300; i++)
                {
                    yield return new WaitForEndOfFrame();
                    
                    // work around to keep profiler working
                    if(!UnityEngine.Profiling.Profiler.enabled) UnityEngine.Profiling.Profiler.enabled = true;
                }
                
                // start again using next file name
                count++;
            }
        }
    }
}


