﻿using UnityEngine;

namespace ProfilerExample
{
	public class CustomTimerUsageExample : MonoBehaviour
	{
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                var numTests = 1000;

                using (new CustomTimer ("My Test", numTests)) 
                {
                    for (int i = 0; i < numTests; i++)
                        TestFunction();
                } 
            }
        }

		void TestFunction()
		{
			// count up until 100000 for no reason..
			for(int i = 0; i < 100000; i++){}
            
		}
	}
}

