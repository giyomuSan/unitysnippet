﻿using System;
using System.Diagnostics;
using System.Collections;

namespace ProfilerExample
{
	public class CustomTimer : IDisposable
	{
		#region fields
		private string timerName;
		private int numTests;
		private Stopwatch watch;
		#endregion

		#region methods
		public CustomTimer(string timerName, int numTests)
		{
			this.timerName = timerName;
			this.numTests = numTests;

			if (numTests < 0) numTests = 1;

			watch = Stopwatch.StartNew();
		}

		// called when the "using" block ends
		public void Dispose()
		{
			watch.Stop ();
			float ms = watch.ElapsedMilliseconds;

			var msg = String.Format("{0} finished : {1:0.00}ms total, {2:0.000000}ms per test for {3} tests", timerName, ms, ms / numTests, numTests);
			UnityEngine.Debug.Log (msg);
		}
		#endregion
	}
}

