﻿using UnityEngine;
using System.Collections.Generic;

namespace ProfilerExample
{
	public class ProfilerScriptControlSample : MonoBehaviour 
	{
		void Update()
		{
			DoSomethingStupid ();
		}

		void DoSomethingStupid()
		{
			UnityEngine.Profiling.Profiler.BeginSample ("My Profiler Sample");

			List<int> listofInts = new List<int> ();
			for(int i = 0; i < 1000000; ++i)
			{
				listofInts.Add (i);
			}

			UnityEngine.Profiling.Profiler.EndSample ();
		}
	}
}
