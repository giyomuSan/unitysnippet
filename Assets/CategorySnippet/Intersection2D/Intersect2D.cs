using System;

#region structures
public struct Vec2D
{
	public float x, y;

	public Vec2D (float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public static Vec2D operator +(Vec2D v1, Vec2D v2)
	{
		return new Vec2D(v1.x + v2.x, v1.y + v2.y);
	}

	public static Vec2D operator -(Vec2D v1, Vec2D v2)
	{
		return new Vec2D(v1.x - v2.x, v1.y - v2.y);
	}

	public static Vec2D operator *(Vec2D v1, float m)
	{
		return new Vec2D(v1.x * m, v1.y * m);
	}

	public float LengthSqr()
	{
		return (x * x + y * y);
	}

	public float Length()
	{
		return (float)Math.Sqrt(x * x + y * y);
	}
}

public struct Point2D
{
	public float x, y;

	public Point2D (float x, float y)
	{
		this.x = x;
		this.y = y;
	}
}

public struct Line2D
{
	public Point2D p1, p2;
}

public struct Circle2D
{
	public Point2D c;
	public float r;
}
#endregion


public static class Intersect2D
{
	#region methods
	public static bool IsCircleToCircleIntersection(Circle2D c1, Circle2D c2, out Point2D[] pIntersection)
	{
		pIntersection = new Point2D[2];

		float a, dist, h;
		Vec2D d, r, v2;

		// distance between circle center (hor & vert)
		d = new Vec2D(c2.c.x - c1.c.x, c2.c.y - c1.c.y);
		dist = d.Length();

		// infinity and equality intersection
		if(dist == 0 && c1.r == c2.r) return false;

		// solvability..

		// no solution , circle do not intersect
		if(dist > c1.r + c2.r) return false;

		// no solution, one circle is contained in teh other
		if(dist < Math.Abs(c1.r - c2.r)) return false;

		// one solution
		if(dist == c1.r + c2.r)
		{
			pIntersection[0] = new Point2D((c1.c.x - c2.c.x) / (c1.r + c2.r) * c1.r + c2.c.x, (c1.c.y - c2.c.y) / (c1.r + c2.r) * c1.r + c2.c.y);
			return true;
		}

		// distance from point 0 to point 2
		a = ((c1.r * c1.r) - (c2.r * c2.r) + (dist * dist)) / (2.0f * dist);

		// point 2 coordinates
		v2 = new Vec2D(c1.c.x + (d.x * a / dist), c1.c.y + (d.y * a / dist));

		// distance from point 2 to either of the intersection point
		h = (float)Math.Sqrt((c1.r * c1.r) - (a * a));

		// offset intersection from point 2
		r = new Vec2D(-d.y * (h / dist), d.x * (h / dist));

		// absolute intersection point
		pIntersection[0] = new Point2D((v2 + r).x, (v2 + r).y);
		pIntersection[1] = new Point2D((v2 - r).x, (v2 - r).y);

		return true;
	}

	public static bool IsLineToLineIntersect(Line2D lhs, Line2D rhs, out Point2D pIntersection)
	{
		pIntersection = new Point2D();
		float r, d;

		// check line aren't parallel
		if(IsParallel(lhs, rhs)) return false;

		d = ((lhs.p2.x - lhs.p1.x) * (rhs.p2.y - rhs.p1.y)) - ((lhs.p2.y - lhs.p1.y) * (rhs.p2.x - rhs.p1.x));
		if(d != 0)
		{
			r = (((lhs.p1.y - rhs.p1.y) * (rhs.p2.x - rhs.p1.x)) - ((lhs.p1.x - rhs.p1.x) * (rhs.p2.y - rhs.p1.y))) / d;

			pIntersection.x = lhs.p1.x + r * (lhs.p2.x - lhs.p1.x);
			pIntersection.y = lhs.p1.y + r * (lhs.p2.y - lhs.p1.y);
		}

		return true;
	}

	public static bool IsLineSegmentToLineSegmentIntersect(Line2D lhs, Line2D rhs, out Point2D pIntersection)
	{
		pIntersection = new Point2D();
		float r, s, d;

		// check line aren't parallel
		if(IsParallel(lhs, rhs)) return false;

		d = ((lhs.p2.x - lhs.p1.x) * (rhs.p2.y - rhs.p1.y)) - ((lhs.p2.y - lhs.p1.y) * (rhs.p2.x - rhs.p1.x));
		if(d != 0)
		{
			r = (((lhs.p1.y - rhs.p1.y) * (rhs.p2.x - rhs.p1.x)) - ((lhs.p1.x - rhs.p1.x) * (rhs.p2.y - rhs.p1.y))) / d;
			s = (((lhs.p1.y - rhs.p1.y) * (lhs.p2.x - lhs.p1.x)) - ((lhs.p1.x - rhs.p1.x) * (lhs.p2.y - lhs.p1.y))) / d;

			if((r >= 0 && r <= 1) && (s >= 0 && s <= 1))
			{
				pIntersection.x = lhs.p1.x + r * (lhs.p2.x - lhs.p1.x);
				pIntersection.y = lhs.p1.y + r * (lhs.p2.y - lhs.p1.y);
			}
		}

		return true;
	}

	public static bool IsLineToCircleIntersection(Line2D line, Circle2D circle, out Point2D[] pIntersection)
	{
		pIntersection = new Point2D[2];

		Vec2D v1, v2;

		// vector for line
		v1 = new Vec2D(line.p2.x - line.p1.x, line.p2.y - line.p1.y);

		// vector from line start point to circle center
		v2 = new Vec2D (circle.c.x - line.p1.x, circle.c.y - line.p1.y);

		float dot = v1.x * v2.x + v1.y * v2.y;
		Vec2D projl = new Vec2D(((dot / (v1.LengthSqr())) * v1.x), ((dot / (v1.LengthSqr())) * v1.y));
		Vec2D midpt = new Vec2D(line.p1.x + projl.x, line.p1.y + projl.y);

		float distToCenter = (midpt.x - circle.c.x) * (midpt.x - circle.c.x) + (midpt.y - circle.c.y) * (midpt.y - circle.c.y);

		if(distToCenter > circle.r * circle.r) return false;

		// 1 solution (tangeant)
		if(distToCenter == circle.r * circle.r)
		{
			pIntersection[0] = new Point2D(midpt.x, midpt.y);
			return true;
		}

		float distToIntersection;

		if(distToCenter == 0) 
			distToIntersection = circle.r;
		else
		{
			distToCenter = (float)Math.Sqrt(distToCenter);
			distToIntersection = (float)Math.Sqrt(circle.r * circle.r - distToCenter * distToCenter);
		}

		float lineSegmentLength = 1f / v1.Length();
		v1 *= lineSegmentLength;
		v1 *= distToIntersection;

		pIntersection[0] = new Point2D(midpt.x + v1.x, midpt.y + v1.y);
		pIntersection[1] = new Point2D(midpt.x - v1.x, midpt.y - v1.y);

		return true;
	}

	static bool IsParallel(Line2D lhs, Line2D rhs)
	{
		return (lhs.p2.y - lhs.p1.y) / (lhs.p2.x - lhs.p1.x) == (rhs.p2.y - rhs.p1.y) / (rhs.p2.x - rhs.p1.x);
	}
	#endregion
}

