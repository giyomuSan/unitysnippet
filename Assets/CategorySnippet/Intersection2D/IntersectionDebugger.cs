﻿using UnityEngine;
using System.Collections;

public class IntersectionDebugger : MonoBehaviour 
{
	#region class
	[System.Serializable]
	public class Line
	{
		public Transform Ps,Pe;

		public bool HasTransform()
		{
			return Ps & Pe;
		}
	}
	#endregion

	#region fields
	public Line L1 = null;
	public Line L2 = null;
	public Transform Pintersect1;
	public Transform Pintersect2;
	public Transform CenterPoint1;
	public float Radius1;
	public Transform CenterPoint2;
	public float Radius2;

	public enum IntersectionType
	{
		Line,
		Segment,
		LineToCircle,
		CircleToCircle
	}
	public IntersectionType Intersection;
	#endregion

	#region methods
	void OnDrawGizmos()
	{
		Gizmos.color = Color.white;

		if(L1.HasTransform() && L2.HasTransform())
		{
			Gizmos.DrawLine(L1.Pe.position, L1.Ps.position);
			Gizmos.DrawLine(L2.Pe.position, L2.Ps.position);

			if(Intersection == IntersectionType.Line)
			{
				var a = MakeLine2D(L1);
				var b = MakeLine2D(L2);
				Point2D p;

				if(Intersect2D.IsLineToLineIntersect(a,b,out p))
					Pintersect1.position = new Vector2(p.x, p.y);

			}
			else if(Intersection == IntersectionType.Segment)
			{
				var a = MakeLine2D(L1);
				var b = MakeLine2D(L2);
				Point2D p;

				if(Intersect2D.IsLineSegmentToLineSegmentIntersect(a,b,out p))
					Pintersect1.position = new Vector2(p.x, p.y);
			}
			else if(Intersection == IntersectionType.LineToCircle)
			{
				if(CenterPoint1 != null)
				{
					Gizmos.DrawWireSphere(CenterPoint1.position, Radius1);

					var line = MakeLine2D(L1);
					var circle = MakeCircle2D(CenterPoint1.position, Radius1);
					Point2D[] p;

					if(Intersect2D.IsLineToCircleIntersection(line, circle, out p))
					{
						Pintersect1.position = new Vector2(p[0].x, p[0].y);
						Pintersect2.position = new Vector2(p[1].x, p[1].y);
					}
				}
			}
			else if(Intersection == IntersectionType.CircleToCircle)
			{
				Gizmos.DrawWireSphere(CenterPoint1.position, Radius1);
				Gizmos.DrawWireSphere(CenterPoint2.position, Radius2);

				var circle1 = MakeCircle2D(CenterPoint1.position, Radius1);
				var circle2 = MakeCircle2D(CenterPoint2.position, Radius2);
				Point2D[] p;

				if(Intersect2D.IsCircleToCircleIntersection(circle1, circle2, out p))
				{
					Pintersect1.position = new Vector2(p[0].x, p[0].y);
					Pintersect2.position = new Vector2(p[1].x, p[1].y);
				}
			}
		}
	}

	Line2D MakeLine2D(Line line)
	{
		return new Line2D
		{
			p1 = new Point2D { x = line.Ps.position.x, y = line.Ps.position.y },
			p2 = new Point2D { x = line.Pe.position.x, y = line.Pe.position.y }
		};
	}

	Circle2D MakeCircle2D(Vector2 center, float radius)
	{
		return new Circle2D
		{
			c = new Point2D(center.x, center.y),
			r = radius
		};
	}
	#endregion
}
