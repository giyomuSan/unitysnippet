#region structures
public struct Point
{
	public float x, y;
}

public struct Segment
{
	public float x1, y1;
	public float x2, y2;
}
#endregion

public static class Intersect
{
	/* used math formula :
	 * Ax + Bx = C
	 * A = y2 - y1
	 * B = x1 - x2
	 * C = A*x1 + B*y1
	 */
	public static Point LineToLine(Segment lhs, Segment rhs, out bool isParallel)
	{
		// first segment
		float la, lb, lc;

		la = lhs.y2 - lhs.y1;
		lb = lhs.x1 - lhs.x2;
		lc = la * lhs.x1 + lb * lhs.y1;

		// second segment
		float ra, rb, rc;

		ra = rhs.y2 - rhs.y1;
		rb = rhs.x1 - rhs.x2;
		rc = ra * rhs.x1 + rb * rhs.y1;

		// line are parallele ?
		float delta = la * rb - ra * lb;
		if(delta == 0f)
		{
			isParallel = true;
			return new Point();
		}

		isParallel = false;
		return new Point
		{
			x = (rb * lc - lb * rc) / delta,
			y = (la * rc - ra * lc) / delta
		};
	}

	public static bool SegmentIntersect(Segment lhs, Segment rhs, out Point intersection)
	{
		float lx, ly, rx, ry, lc, rc;

		lx = lhs.x2 - lhs.x1;
		ly = lhs.y2 - lhs.y1;

		rx = rhs.x2 - rhs.x1;
		ry = rhs.y2 - rhs.y1;

		lc = (-ly * (lhs.x1 - rhs.x1) + lx * (lhs.y1 - rhs.y1)) / (-rx * ly + lx * ry);
		rc = (rx * (lhs.y1 - rhs.y1) - ry * (lhs.x1 - rhs.x1)) / (-rx * ly + lx * ry);

		if(lc >= 0 && lc <= 1 && rc >= 0 && rc <= 1)
		{
			var ipx = lhs.x1 + (rc * lx);
			var ipy = lhs.y1 + (rc * ly);

			intersection = new Point
			{
				x = ipx,
				y = ipy
			};

			return true;
		}

		intersection = new Point();
		return false;
	}

	/*
	public static bool Intersects2D(Segment otherLineSegment, out Point intersectionPoint)
	{
		float firstLineSlopeX, firstLineSlopeY, secondLineSlopeX, secondLineSlopeY;
		
		firstLineSlopeX = this.Point2.X - this.Point1.X;
		firstLineSlopeY = this.Point2.Y - this.Point1.Y;
		
		secondLineSlopeX = otherLineSegment.Point2.X - otherLineSegment.Point1.X;
		secondLineSlopeY = otherLineSegment.Point2.Y - otherLineSegment.Point1.Y;
		
		float s, t;
		s = (-firstLineSlopeY * (this.Point1.X - otherLineSegment.Point1.X) + firstLineSlopeX * (this.Point1.Y - otherLineSegment.Point1.Y)) / (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);
		t = (secondLineSlopeX * (this.Point1.Y - otherLineSegment.Point1.Y) - secondLineSlopeY * (this.Point1.X - otherLineSegment.Point1.X)) / (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);
		
		if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
		{
			float intersectionPointX = this.Point1.X + (t * firstLineSlopeX);
			float intersectionPointY = this.Point1.Y + (t * firstLineSlopeY);
			
			// Collision detected
			intersectionPoint = new Vector3(intersectionPointX, intersectionPointY, 0);
			
			return true;
		}
		
		intersectionPoint = Vector3.Zero;
		return false; // No collision
	}
	*/
}

