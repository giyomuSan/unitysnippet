﻿using UnityEngine;
using System.Collections;

public class QuadraticBezierCurve : MonoBehaviour
{
    #region fields
    public GameObject StartPoint;
    public GameObject MiddlePoint;
    public GameObject EndPoint;
    public GameObject MovePoint;
    public float Duration = 2f;
    public float Width = 0.2f;
    public int NumberOfPoints = 20;
    public bool Move;

    private Color color = Color.white;
    private float progress;
    Vector3[] savedPosition;
    #endregion

    #region methods
    void Start ()
    {
        // Line renderer component
        InitializeLineRenderer();

        savedPosition = new Vector3[NumberOfPoints];
    }
	
	void Update ()
    {
        var line = GetComponent<LineRenderer>();
        if (line == null || StartPoint == null || MiddlePoint == null || EndPoint == null)
            return;

        line.SetColors(color, color);
        line.SetWidth(Width, Width);
        if (NumberOfPoints > 0)
            line.SetVertexCount(NumberOfPoints);

        // setting quadratic bezier curve points
        var p0 = StartPoint.transform.position;
        var p1 = MiddlePoint.transform.position;
        var p2 = EndPoint.transform.position;
        float t;
        Vector3 position = Vector3.zero;

        for(int i = 0; i < NumberOfPoints; i++)
        {
            t = (float)i / (NumberOfPoints - 1f);

            // formula : B(t) = (1 - t)^2P0+2(1-t)tP1+t^2P2
            position = (1f - t) * (1f - t) * p0 + 2f * (1f - t) * t * p1 + t * t * p2;
            line.SetPosition(i, position);
            savedPosition[i] = position;
        }

        ConnectPointDebug(p0, p1, p2);

        if(Move)
        {
            Move = false;
            StartCoroutine(MoveAlongCurve());
        }
	}

    IEnumerator MoveAlongCurve()
    {
        var count = 0;
        while(count < NumberOfPoints)
        {
            MovePoint.transform.position = Vector3.Lerp(savedPosition[count], savedPosition[(count+1) % NumberOfPoints], Time.deltaTime);
            //Debug.Log(savedPosition[count % NumberOfPoints]);
            yield return new WaitForSeconds(Duration);
            count++;
        }
    }

    void InitializeLineRenderer()
    {
        LineRenderer line = GetComponent<LineRenderer>();
        if (line == null)
            gameObject.AddComponent<LineRenderer>();

        line = GetComponent<LineRenderer>();
        line.useWorldSpace = true;
        line.material = new Material(Shader.Find("Particles/Additive"));
    }

    void ConnectPointDebug(Vector3 p0, Vector3 p1, Vector3 p2)
    {
        Debug.DrawLine(p0, p1, Color.red);
        Debug.DrawLine(p1, p2, Color.red);
    }
    #endregion
}
