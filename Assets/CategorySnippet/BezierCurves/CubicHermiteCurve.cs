﻿using UnityEngine;
using System.Collections;

public class CubicHermiteCurve : MonoBehaviour
{
    #region fields
    public GameObject StartPoint;
    public GameObject StartTangentPoint;
    public GameObject EndPoint;
    public GameObject EndTangentPoint;
    public int NumberOfPoints = 20;
    public float Width = 0.2f;

    private Color color = Color.white;
    #endregion

    #region methods
    void Start ()
    {
        // Line renderer component
        InitializeLineRenderer();
    }
	
	void Update ()
    {
        // parameters and component check
        var line = GetComponent<LineRenderer>();
        if (line == null || StartPoint == null || StartTangentPoint == null || EndPoint == null || EndTangentPoint == null)
            return;

        // line renderer update
        line.SetColors(color, color);
        line.SetWidth(Width, Width);
        if (NumberOfPoints > 0)
            line.SetVertexCount(NumberOfPoints);

        // setting hermite point curves
        Vector3 p0 = StartPoint.transform.position;
        Vector3 p1 = EndPoint.transform.position;
        Vector3 m0 = StartTangentPoint.transform.position - StartPoint.transform.position;
        Vector3 m1 = EndTangentPoint.transform.position - EndPoint.transform.position;
        float t;
        Vector3 position;

        for(int i = 0; i < NumberOfPoints; i++)
        {
            t = (float)i / (NumberOfPoints - 1f);
            position = (2f * t * t * t - 3f * t * t * t + 1f) * p0 
                        + (t * t * t - 2f * t * t + t) * m0 
                        + (-2f * t * t * t + 3f * t * t) * p1 
                        + (t * t * t - t * t) * m1;

            line.SetPosition(i, position);
        }

        ConnectPointDebug(p0, StartTangentPoint.transform.position, p1, EndTangentPoint.transform.position);
    }

    void InitializeLineRenderer()
    {
        LineRenderer line = GetComponent<LineRenderer>();
        if (line == null)
            gameObject.AddComponent<LineRenderer>();

        line = GetComponent<LineRenderer>();
        line.useWorldSpace = true;
        line.material = new Material(Shader.Find("Particles/Additive"));
    }

    void ConnectPointDebug(Vector3 p0, Vector3 m0, Vector3 p1, Vector3 m1)
    {
        Debug.DrawLine(p0, m0, Color.red);
        Debug.DrawLine(p1, m1, Color.red);
    }
    #endregion
}
