﻿using UnityEngine;
using System.Collections;

public class QuadraticBezierSpline : MonoBehaviour
{
    #region fields
    public GameObject[] ControlPoints;
    public float Width = 0.2f;
    public int NumberOfPoints = 20;

    private Color color = Color.white;
    #endregion

    #region methods
    void Start ()
    {
        // Line renderer component
        InitializeLineRenderer();
    }
	
	void Update ()
    {
        var line = GetComponent<LineRenderer>();
        if (line == null || ControlPoints == null || ControlPoints.Length < 3)
            return;

        line.SetColors(color, color);
        line.SetWidth(Width, Width);
        if (NumberOfPoints < 2)
            NumberOfPoints = 2;

        line.SetVertexCount(NumberOfPoints * (ControlPoints.Length - 2));

        Vector3 p0, p1, p2;

        for(int i = 0; i < ControlPoints.Length - 2; i++)
        {
            // check control point
            if(ControlPoints[i] == null || ControlPoints[i + 1] == null || ControlPoints[i + 2] == null)
                return;

            // determine control points of segment
            p0 = 0.5f * (ControlPoints[i].transform.position + ControlPoints[i + 1].transform.position);
            p1 = ControlPoints[i + 1].transform.position; 
            p2 = 0.5f * (ControlPoints[i + 1].transform.position + ControlPoints[i + 2].transform.position);

            //Debug.DrawRay(p2, Vector3.up * 3f, Color.blue);

            // set points of quadratic bezier curve
            Vector3 position;
            float t;
            float pointStep = 1f / NumberOfPoints;

            if(i == ControlPoints.Length - 3)
            {
                // last point of last segment should reach p2
                pointStep = 1f / (NumberOfPoints - 1);
            }

            for(int j = 0; j < NumberOfPoints; j++)
            {
                t = j * pointStep;
                position = (1f - t) * (1f - t) * p0 + 2f * (1f - t) * t * p1 + t * t * p2;
                line.SetPosition(j + i * NumberOfPoints, position);

                //Debug.DrawRay(position, Vector3.up, Color.gray * 3f);
            }
        }

        ConnectDebugPoint();
    }

    void InitializeLineRenderer()
    {
        LineRenderer line = GetComponent<LineRenderer>();
        if (line == null)
            gameObject.AddComponent<LineRenderer>();

        line = GetComponent<LineRenderer>();
        line.useWorldSpace = true;
        line.material = new Material(Shader.Find("Particles/Additive"));
    }

    void ConnectDebugPoint()
    {
        for(int i = 0; i < ControlPoints.Length-1; i++)
        {
      
            Debug.DrawLine(ControlPoints[i].transform.position, ControlPoints[i+1].transform.position, Color.green);
        }
    }
    #endregion
}
