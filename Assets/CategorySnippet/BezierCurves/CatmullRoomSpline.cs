﻿using UnityEngine;
using System.Collections;

public class CatmullRoomSpline : MonoBehaviour
{
    #region fields
    public GameObject[] ControlPoints;
    public float Width = 0.2f;
    public int NumberOfPoints = 20;

    private Color color = Color.white;
    #endregion

    #region methods
    void Start ()
    {
        // Line renderer component
        InitializeLineRenderer();
    }
	
	void Update ()
    {
        // parameters and component check
        var line = GetComponent<LineRenderer>();
        if (line == null || ControlPoints == null || ControlPoints.Length < 2)
            return;

        // line renderer update
        line.SetColors(color, color);
        line.SetWidth(Width, Width);
        if (NumberOfPoints < 2)
            NumberOfPoints = 2;

        line.SetVertexCount(NumberOfPoints * (ControlPoints.Length - 1));

        // loop over segment of spline
        Vector3 p0, m0, p1, m1;

        for(int j = 0; j < ControlPoints.Length - 1; j++)
        {
            // check control points
            if (ControlPoints[j] == null || ControlPoints[j + 1] == null || 
                (j > 0 && ControlPoints[j-1] == null) || (j < ControlPoints.Length - 2 && ControlPoints[j+2] == null))
                return;

            // determine control point of segment
            p0 = ControlPoints[j].transform.position;
            p1 = ControlPoints[j + 1].transform.position;

            if(j > 0)
            {
                m0 = 0.5f * (ControlPoints[j + 1].transform.position - ControlPoints[j - 1].transform.position);
            }
            else
            {
                m0 = ControlPoints[j + 1].transform.position - ControlPoints[j].transform.position;
            }

            if (j < ControlPoints.Length - 2)
            {
                m1 = 0.5f * (ControlPoints[j + 2].transform.position - ControlPoints[j].transform.position);
            }
            else
            {
                m1 = ControlPoints[j + 1].transform.position - ControlPoints[j].transform.position;
            }

            // set point of hermite curve
            Vector3 position;
            float t;
            float pointStep = 1f / NumberOfPoints;

            if (j == ControlPoints.Length - 2)
            {
                // last point of last segment should reach p2
                pointStep = 1f / (NumberOfPoints - 1);
            }

            for(int i = 0; i < NumberOfPoints; i++)
            {
                t = i * pointStep;
                position = (2f * t * t * t - 3f * t * t * t + 1f) * p0
                        + (t * t * t - 2f * t * t + t) * m0
                        + (-2f * t * t * t + 3f * t * t) * p1
                        + (t * t * t - t * t) * m1;

                line.SetPosition(i + j * NumberOfPoints, position);
            }
        }

        ConnectDebugPoint();
    }

    void InitializeLineRenderer()
    {
        LineRenderer line = GetComponent<LineRenderer>();
        if (line == null)
            gameObject.AddComponent<LineRenderer>();

        line = GetComponent<LineRenderer>();
        line.useWorldSpace = true;
        line.material = new Material(Shader.Find("Particles/Additive"));
    }

    void ConnectDebugPoint()
    {
        for (int i = 0; i < ControlPoints.Length - 1; i++)
        {
            Debug.DrawLine(ControlPoints[i].transform.position, ControlPoints[i + 1].transform.position, Color.green);
        }
    }
    #endregion
}
