using UnityEngine;
using System.Collections;

public class CubePrefab : MonoBehaviour
{
	public GenericPoolSampleUsage.TriggerEventCallback callback;
	private float fallSpeed;
	
	void OnEnable()
	{
		fallSpeed = Random.Range(1f, 5f);
	}

	void Update()
	{
		transform.Translate(-Vector3.up * Time.deltaTime * fallSpeed, Space.World);
	}

	void OnTriggerEnter()
	{
		callback(gameObject);
	}
}

