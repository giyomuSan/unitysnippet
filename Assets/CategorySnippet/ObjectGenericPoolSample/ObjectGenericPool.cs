﻿using System;
using System.Collections.Generic;

public class ObjectPoolGeneric<T>
{
	Stack<T> store = new Stack<T>();
	
	public int MaxStored;
	public Func<T> CreateLogic;
	public Func<T, T> SaveLogic;
	public Action<T> DestroyLogic;
	
	public T Get()
	{
		if (store.Count > 0) return store.Pop();
		else if (CreateLogic != null) return CreateLogic();
		return default(T);
	}
	
	public void Save(T item)
	{
		if (store.Count < MaxStored)
		{
			if (SaveLogic != null) item = SaveLogic(item);
			store.Push(item);
		}
		else if (DestroyLogic != null) DestroyLogic(item);
	}
}
