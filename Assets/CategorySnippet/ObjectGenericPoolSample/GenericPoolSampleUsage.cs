﻿using UnityEngine;
using System.Collections;

public class GenericPoolSampleUsage : MonoBehaviour 
{
	public GameObject prefab;
	public int PreInstanceCount;
	public int MaxInstances;

	private ObjectPoolGeneric<GameObject> pool;

	public delegate void TriggerEventCallback(GameObject go);
	//TriggerEventCallback del;
	
	void Start () 
	{
		pool = new ObjectPoolGeneric<GameObject>()
		{
			MaxStored = MaxInstances,
			CreateLogic = () => CreateInstance(),
			SaveLogic = (go) => go,
			DestroyLogic = go => Destroy(go)
		};

		for(int i = 0; i < PreInstanceCount; i++)
		{
			var go = CreateInstance();
			go.SetActive(false);
			pool.Save(go);
		}
	}

	GameObject CreateInstance()
	{
		var instance = Instantiate<GameObject>(prefab);

		instance.AddComponent<CubePrefab>().callback = (go) => {
			go.transform.position = Vector3.zero;
			go.SetActive(false);
			pool.Save(go);
		};

		return instance;
	}

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Space))
			pool.Get().SetActive(true);
	}
}
