﻿using UnityEngine;
using UnityEngine.Audio;

namespace AudioMixerTest
{
	public class SnapshotTest : MonoBehaviour 
	{
		public AudioMixerSnapshot SfxOn;
		public AudioMixerSnapshot SfxOff;

		private bool flag;

		void Update()
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				flag = !flag;
				Attenuate();
			}
		}

		void Attenuate()
		{
			if(flag) SfxOff.TransitionTo(2f);
			else SfxOn.TransitionTo(.5f);
		}
	}
}
