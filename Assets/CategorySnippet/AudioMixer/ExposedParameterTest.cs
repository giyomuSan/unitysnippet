using UnityEngine;
using UnityEngine.Audio;


public class ExposedParameterTest : MonoBehaviour
{
	public AudioMixer MasterMixer;

	public void SetMasterVolume(float level)
	{
		MasterMixer.SetFloat ("MasterVolume", level);
	}
}


