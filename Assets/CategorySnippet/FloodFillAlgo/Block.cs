using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace FloodFill
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class Block : MonoBehaviour, IPointerClickHandler
	{
		private SpriteRenderer spRenderer;
		private static Grid grid;

		public void SetName(string name)
		{
			gameObject.name = name;
		}

		public void SetColor(Color32 color)
		{
			if(spRenderer == null)
				spRenderer = GetComponent<SpriteRenderer>();

			spRenderer.color = color;
		}

		public Color32 GetColor()
		{
			return spRenderer.color;
		}

		public void SetPosition(Vector2 position)
		{
			transform.position = position;
		}

		public Vector2 GetPosition()
		{
			return transform.position;
		}

		public void OnPointerClick (PointerEventData eventData)
		{
			Debug.Log(gameObject.name +": "+GetColor() +" <> "+ GetPosition());

			if(grid == null) 
				grid = FindObjectOfType(typeof(Grid)) as Grid;

			if(grid)
			{
				//grid.FloodFillArea(this);
				grid.FloodFillBorder(this);
			}
		}
	}
}

