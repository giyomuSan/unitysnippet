﻿using UnityEngine;
using System.Collections.Generic;

namespace FloodFill
{
	public class Grid : MonoBehaviour 
	{
		public GameObject Square;
		public int Rows, Columns;
		public Color32[] RandomColors;
		public Color32 fillColor;
		public Color32 borderColor;

		private Block[,] gridArray;
		private const float unit = 32f;

		void Start()
		{
			gridArray = new Block[Rows,Columns];

			for(int x = 0; x < Rows; x++)
			{
				for(int y = 0; y < Columns; y++)
				{
					var block = Instantiate(Square).GetComponent<Block>();

					var rand = Random.Range(0, RandomColors.Length);
					block.SetColor(RandomColors[rand]);
					block.SetPosition(new Vector2(x, y));
					block.SetName("block ["+x.ToString()+","+y.ToString()+"]");

					gridArray[x,y] = block;
				}
			}
		}


		public void FloodFillArea(Block block)
		{
			Color32 refColor = block.GetColor();
			Queue<Block> nodes = new Queue<Block>();
			nodes.Enqueue(block);

			while(nodes.Count > 0)
			{
				Block current = nodes.Dequeue();

				var cx = (int)current.GetPosition().x;
				var cy = (int)current.GetPosition().y;

				for(int i = cx; i < Rows; i++)
				{
					for(int j = cy; j < Columns; j++)
					{
						if(!IsSameColor(current.GetColor(), refColor) || IsSameColor(current.GetColor(),fillColor))
							break;

						current.SetColor(fillColor);

						if(i-1 >= 0)
						{
							var westBlock = gridArray[i-1,j];
							if(IsSameColor(westBlock.GetColor(), refColor) && !IsSameColor(westBlock.GetColor(), fillColor))
								nodes.Enqueue(westBlock);
						}

						if(i+1 < Rows)
						{
							var eastBlock = gridArray[i+1,j];
							if(IsSameColor(eastBlock.GetColor(), refColor) && !IsSameColor(eastBlock.GetColor(), fillColor))
								nodes.Enqueue(eastBlock);
						}

						if(j+1 < Columns)
						{
							var northBlock = gridArray[i,j+1];
							if(IsSameColor(northBlock.GetColor(), refColor) && !IsSameColor(northBlock.GetColor(), fillColor))
								nodes.Enqueue(northBlock);
						}
						
						if(j-1 >= 0)
						{
							var southBlock = gridArray[i,j-1];
							if(IsSameColor(southBlock.GetColor(), refColor) && !IsSameColor(southBlock.GetColor(), fillColor))
								nodes.Enqueue(southBlock);
						}
					}
				}
			}
		}

		public void FloodFillBorder(Block block)
		{
			bool[,] checkedBlock = new bool[Rows,Columns];
			Queue<Block> nodes = new Queue<Block>();
			nodes.Enqueue(block);

			while (nodes.Count > 0) 
			{
				Block current = nodes.Dequeue();
				
				var cx = (int)current.GetPosition().x;
				var cy = (int)current.GetPosition().y;

				for(int i = cx; i < Rows; i++)
				{
					for(int j = cy; j < Columns; j++)
					{
						// Use cx, and cy for checked block as you want to check against current node.
						if(checkedBlock[cx,cy] == true || IsSameColor(current.GetColor(), borderColor)) 
							break;
						
						current.SetColor(fillColor);
						checkedBlock[cx,cy] = true;

						if(i-1 >= 0)
						{
							if(checkedBlock[i-1,j] == false && !IsSameColor(gridArray[i-1,j].GetColor(), borderColor))
								nodes.Enqueue(gridArray[i-1,j]);
						}


						if(i+1 < Rows)
						{
							if(checkedBlock[i+1,j] == false && !IsSameColor(gridArray[i+1,j].GetColor(), borderColor))
								nodes.Enqueue(gridArray[i+1,j]);
						}

						if(j+1 < Columns)
						{
							if(checkedBlock[i,j+1] == false && !IsSameColor(gridArray[i,j+1].GetColor(), borderColor))
								nodes.Enqueue(gridArray[i,j+1]);
						}
						
						if(j-1 >= 0)
						{
							if(checkedBlock[i,j-1] == false && !IsSameColor(gridArray[i,j-1].GetColor(), borderColor))
								nodes.Enqueue(gridArray[i,j-1]);
						}
					}
				}
			}
		}

		private bool IsSameColor(Color32 lhs, Color32 rhs)
		{
			return lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b;
		}
	}
}

