﻿using UnityEngine;
using System.Collections.Generic;

namespace ScriptableObjectTest
{
    public class UseMyScriptableObject : MonoBehaviour
    {
        public MyScriptableObjectClass MyScriptableObject;
        private List<Light> myLights;

        void Start()
        {
            myLights = new List<Light>();
            foreach(var spawn in MyScriptableObject.SpawnPoints)
            {
                GameObject myLight = new GameObject("Light");
                myLight.AddComponent<Light>();
                myLight.transform.position = spawn;
                myLight.GetComponent<Light>().enabled = false;

                if(MyScriptableObject.ColorIsRandom)
                {
                    myLight.GetComponent<Light>().color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
                }
                else
                {
                    myLight.GetComponent<Light>().color = MyScriptableObject.ThisColor;
                }
                myLights.Add(myLight.GetComponent<Light>());
            }
        }

        void Update()
        {
            if(Input.GetButtonDown("Fire1"))
            {
                foreach(var light in myLights)
                {
                    light.enabled = true;
                }
            }

            if(Input.GetButtonDown("Fire2"))
            {
                UpdateLights();
            }
        }

        void UpdateLights()
        {
            foreach (var myLight in myLights)
            {
                myLight.GetComponent<Light>().color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
            }
        }
    }
}
