﻿using UnityEngine;
using System.Collections.Generic;

namespace ScriptableObjectTest
{
    [System.Serializable]
    public abstract class InventoryItem : ScriptableObject
    {
        public string ItemName;
        public Texture2D Icon;
        public GameObject ItemObject;
    }

    [CreateAssetMenu(fileName = "HealPotion", menuName = "Inventory/Item/HealPotion", order = 3)]
    public class HealPotionItem : InventoryItem
    {
        public int HealPower;
    }

    [CreateAssetMenu(fileName = "InventoryList", menuName = "Inventory/ItemList", order = 2)]
    public class InventoryItemList : ScriptableObject
    {
        public List<InventoryItem> ItemList = new List<InventoryItem>();
    }
}
