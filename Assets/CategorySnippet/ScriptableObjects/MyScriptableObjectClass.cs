﻿using UnityEngine;
using System.Collections;

namespace ScriptableObjectTest
{
    // this attribute will allow to create asset now from Asset menu
    [CreateAssetMenu(fileName = "Data", menuName = "Inventory/List", order = 1)]
    public class MyScriptableObjectClass : ScriptableObject
    {
        public string ObjectName = "New MyScriptableObjectClass";
        public bool ColorIsRandom = false;
        public Color ThisColor = Color.white;
        public Vector3[] SpawnPoints;
    }
}
