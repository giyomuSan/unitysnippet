﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FSM.Deterministic
{
    public abstract class AIEntity : MonoBehaviour
    {
        protected FsmSystem fsm;

        public abstract void CreateFsm();

        public virtual void SetTransition(Transition transition)
        {
            fsm.PerformTransition(transition);
        }
    }

    public abstract class FsmState
    {
        #region fields
        protected Dictionary<Transition, StateID> map = new Dictionary<Transition, StateID>();
        protected StateID stateID;
        #endregion

        #region properties
        public StateID ID { get { return stateID; } }
        #endregion

        #region methods
        public void AddTransition(Transition transition, StateID id)
        {
            if (IsNullTransition(transition) || IsNullState(id)) return;

            if(map.ContainsKey(transition))
            {
                Debug.LogError("FsmState ERROR : State " + stateID.ToString() + " already has transition " + transition.ToString() + " impossible to assign to another state");
                return;
            }

            map.Add(transition, id);
        }

        public void DeleteTransition(Transition transition)
        {
            if (IsNullTransition(transition)) return;

            if(map.ContainsKey(transition))
            {
                map.Remove(transition);
                return;
            }

            Debug.LogError("FsmState ERROR : Transition " + transition.ToString() + " passed to " + stateID.ToString() + "was not on the state's transition list");
        }

        public StateID GetOutputState(Transition transition)
        {
            if(map.ContainsKey(transition))
            {
                return map[transition];
            }

            return StateID.NullStateID;
        }

        public virtual void DoBeforeEntering() { }
        public virtual void DoBeforeLeaving() { }

        public abstract void Condition(GameObject player, GameObject npc);
        public abstract void Action(GameObject player, GameObject npc);

        bool IsNullTransition(Transition transition)
        {
            if(transition == Transition.NullTransition)
            {
                Debug.LogError("FSMState ERROR : NullTransition are not allowed");
                return true;
            }

            return false;
        }

        bool IsNullState(StateID id)
        {
            if(id == StateID.NullStateID)
            {
                Debug.LogError("FSMState ERROR : NullStateID are not allowed");
                return true;
            }

            return false;
        }
        #endregion
    }

    public class FsmSystem
    {
        #region fields
        private List<FsmState> states;
        private StateID currentStateID;
        private FsmState currentState;
        #endregion

        #region properties
        public StateID CurrentStateID { get { return currentStateID; } }
        public FsmState CurrentState { get { return currentState; } }
        #endregion

        #region methods
        public FsmSystem()
        {
            states = new List<FsmState>();
        }

        public void AddState(FsmState state)
        {
            if(state == null)
            {
                Debug.LogError("FSM ERROR : Null reference is not allowed");
                return;
            }

            if(states.Count == 0)
            {
                states.Add(state);
                currentState = state;
                currentStateID = state.ID;
                return;
            }

            foreach(FsmState s in states)
            {
                Debug.Log(s.ID + "//" + s.GetType().ToString());
                if(s.ID == state.ID)
                {
                    Debug.LogError("FSM ERROR : Impossible to add state " + state.ID.ToString() + " because it has already been added");
                    return;
                }
            }

            states.Add(state);
        }

        public void DeleteState(StateID id)
        {
            if(id == StateID.NullStateID)
            {
                Debug.Log("FSM ERROR : NullStateID is not allowed");
                return;
            }

            foreach(FsmState s in states)
            {
                if(s.ID == id)
                {
                    states.Remove(s);
                    return;
                }

                Debug.LogError("FSM ERROR : Impossible to delete state " + id.ToString() + ". It was not on the list of states");
            }
        }

        public void PerformTransition(Transition transition)
        {
            if(transition == Transition.NullTransition)
            {
                Debug.Log("FSM ERROR : NullTransition is not allowed");
                return;
            }

            var id = currentState.GetOutputState(transition);
            if(id == StateID.NullStateID)
            {
                Debug.LogError("FSM ERROR : State " + currentStateID.ToString() + " does not have a target state for transition " + transition.ToString());
                return;
            }

            currentStateID = id;
            foreach(FsmState s in states)
            {
                if(s.ID == currentStateID)
                {
                    currentState.DoBeforeLeaving();
                    currentState = s;

                    currentState.DoBeforeEntering();
                    break;
                }
            }
        }
        #endregion
    }
}
