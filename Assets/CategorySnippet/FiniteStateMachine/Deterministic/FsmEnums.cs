﻿namespace FSM.Deterministic
{
    public enum Transition
    {
        NullTransition = 0,
        TargetInSight,
        TargetLost
    }

    public enum StateID
    {
        NullStateID = 0,
        Wander,
        Chase
    }
}
