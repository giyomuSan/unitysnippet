﻿using UnityEngine;
using System.Collections;
using FSM.Deterministic;
using System;

public class NpcControlExample : AIEntity
{
    #region fields
    public GameObject Player;

    #endregion

    #region methods
    void Awake()
    {
        CreateFsm();
    }

    void OnEnable()
    {
        StartCoroutine(DoUpdate());
    }

    public override void SetTransition(Transition transition)
    {
        base.SetTransition(transition);
    }

    public override void CreateFsm()
    {
        var wander = new Wander(this);
        wander.AddTransition(Transition.TargetInSight, StateID.Chase);

        var chase = new Chase(this);
        chase.AddTransition(Transition.TargetLost, StateID.Wander);

        fsm = new FsmSystem();
        fsm.AddState(wander);
        fsm.AddState(chase);
    }

    IEnumerator DoUpdate()
    {
        while(enabled)
        {
            fsm.CurrentState.Condition(Player, gameObject);
            fsm.CurrentState.Action(Player, gameObject);
            yield return null;
        }
    }

    
    #endregion
}

public class Wander : FsmState
{
    NpcControlExample owner;

    public Wander(NpcControlExample owner)
    {
        Debug.Log("In Wander state class constructor");
        stateID = StateID.Wander;
        this.owner = owner;
    }

    public override void Action(GameObject player, GameObject npc)
    {
        Debug.Log("I am wandering " + owner.gameObject.name);
    }

    public override void Condition(GameObject player, GameObject npc)
    {
        var distance = Vector3.Distance(player.transform.position, npc.transform.position);
        if (distance < 5f)
            owner.SetTransition(Transition.TargetInSight);
    }
}

public class Chase : FsmState
{ 
    NpcControlExample owner;

    public Chase(NpcControlExample owner)
    {
        Debug.Log("In Chase state class constructor");
        stateID = StateID.Chase;
        this.owner = owner;
    }

    public override void Action(GameObject player, GameObject npc)
    {
        Debug.Log("I am a chasing " + owner.gameObject.name);
    }

    public override void Condition(GameObject player, GameObject npc)
    {
        var distance = Vector3.Distance(player.transform.position, npc.transform.position);
        if (distance > 5f)
           owner.SetTransition(Transition.TargetLost);
    }
}

