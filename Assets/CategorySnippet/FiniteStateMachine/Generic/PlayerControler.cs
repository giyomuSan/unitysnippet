using UnityEngine;
using System.Collections;

public class PlayerControler : MonoBehaviour
{
	public readonly FiniteStateMachine<PlayerControler> fsm = new FiniteStateMachine<PlayerControler>();

	// Use this for initialization
	IEnumerator Start ()
	{
		fsm.Initialize(this, Walking.Instance);
		var interval = 0.5f;

		while(true)
		{
			fsm.Run();
			yield return new WaitForSeconds(interval);
		}
	}
}

