using UnityEngine;

public class Hungry : IStateFsm<PlayerControler>
{
	#region singleton implemtation
	private static readonly Hungry _instance = new Hungry();
	public static Hungry Instance { get { return _instance; } }
	static Hungry() { }
	private Hungry() { }
	#endregion

	#region interface implementation
	void IStateFsm<PlayerControler>.OnEnter(PlayerControler player)
	{
		Debug.Log("I'am hungry..");

	}

	void IStateFsm<PlayerControler>.OnExecute(PlayerControler player)
	{
		var food = Random.value;

		if(food > 0.8) player.fsm.ChangeState(Thirsty.Instance);
		else player.fsm.ChangeState(Walking.Instance);
	}

	void IStateFsm<PlayerControler>.OnExit(PlayerControler player)
	{
		Debug.Log("I'am full..");
	}

	void IStateFsm<PlayerControler>.Reset(PlayerControler player)
	{
	}
	#endregion
}

public class Thirsty : IStateFsm<PlayerControler>
{
	#region singleton implemtation
	private static readonly Thirsty _instance = new Thirsty();
	public static Thirsty Instance { get { return _instance; } }
	static Thirsty() { }
	private Thirsty() { }
	#endregion
	
	#region interface implementation
	void IStateFsm<PlayerControler>.OnEnter(PlayerControler player)
	{
		Debug.Log("I'am thirsty..");
	}
	
	void IStateFsm<PlayerControler>.OnExecute(PlayerControler player)
	{
		var liquid = Random.value;
		
		if(liquid > 0.4) player.fsm.ChangeState(Walking.Instance);
		else player.fsm.ChangeState(Hungry.Instance);
	}
	
	void IStateFsm<PlayerControler>.OnExit(PlayerControler player)
	{
		Debug.Log("I've drink enough..");
	}
	
	void IStateFsm<PlayerControler>.Reset(PlayerControler player)
	{
	}
	#endregion
}

public class Walking : IStateFsm<PlayerControler>
{
	#region singleton implemtation
	private static readonly Walking _instance = new Walking();
	public static Walking Instance { get { return _instance; } }
	static Walking() { }
	private Walking() { }
	#endregion
	
	#region interface implementation
	void IStateFsm<PlayerControler>.OnEnter(PlayerControler player)
	{
		Debug.Log("I need exercises..");
	}
	
	void IStateFsm<PlayerControler>.OnExecute(PlayerControler player)
	{
		var distance = Random.value;

		if(distance > 0.4f && distance < 0.7f) player.fsm.ChangeState(Thirsty.Instance);
		else if(distance > 0.8f) player.fsm.ChangeState(Sleepy.Instance);
	}
	
	void IStateFsm<PlayerControler>.OnExit(PlayerControler player)
	{
	}
	
	void IStateFsm<PlayerControler>.Reset(PlayerControler player)
	{
	}
	#endregion
}

public class Sleepy : IStateFsm<PlayerControler>
{
	#region singleton implemtation
	private static readonly Sleepy _instance = new Sleepy();
	public static Sleepy Instance { get { return _instance; } }
	static Sleepy() { }
	private Sleepy() { }
	#endregion
	
	#region interface implementation
	void IStateFsm<PlayerControler>.OnEnter(PlayerControler player)
	{
		Debug.Log("I'am so sleepy..");
	}
	
	void IStateFsm<PlayerControler>.OnExecute(PlayerControler player)
	{
		var time = Random.value;
		
		if(time > 0.8) player.fsm.ChangeState(Hungry.Instance);
		else player.fsm.RevertToPreviousState();
	}
	
	void IStateFsm<PlayerControler>.OnExit(PlayerControler player)
	{
	}
	
	void IStateFsm<PlayerControler>.Reset(PlayerControler player)
	{
	}
	#endregion
}





