﻿public interface IStateFsm<T>
{
	void OnEnter(T e);
	void OnExecute(T e);
	void OnExit(T e);
	void Reset(T e);
}

public class FiniteStateMachine<T>
{
	#region fields
	T owner;
	IStateFsm<T> currentState = null;
	IStateFsm<T> previousState = null;
	IStateFsm<T> globalState = null;
	#endregion
	
	#region properties
	public IStateFsm<T> CurrentState { get { return currentState; } }
	public IStateFsm<T> PreviousState { get { return previousState; } }
	#endregion
	
	public void Initialize(T owner, IStateFsm<T> initialState)
	{
		this.owner = owner;
		ChangeState(initialState);
	}
	
	public void Run()
	{
		if (globalState != null) globalState.OnExecute(owner);
		if (currentState != null) currentState.OnExecute(owner);
	}
	
	public void ChangeState(IStateFsm<T> newState)
	{
		previousState = currentState;
		
		if (currentState != null) currentState.OnExit(owner);
		currentState = newState;
		
		if (currentState != null) currentState.OnEnter(owner);
	}
	
	public void RevertToPreviousState()
	{
		if (previousState != null) ChangeState(previousState);
	}
	
	public void Reset()
	{
		if (currentState != null) currentState.Reset(owner);
	}
}