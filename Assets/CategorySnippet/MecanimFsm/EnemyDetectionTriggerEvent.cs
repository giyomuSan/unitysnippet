﻿using UnityEngine;

public class EnemyDetectionTriggerEvent : MonoBehaviour
{
    readonly int IsEnemyDetected = Animator.StringToHash("IsEnemyDetected");

    void OnTriggerEnter(Collider c)
    {
        var animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.GetBehaviour<SeekBehaviour>().targetTransform = c.transform;
            animator.SetBool(IsEnemyDetected, true);
        }
    }

    void OnTriggerExit(Collider c)
    {
        var animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetBool(IsEnemyDetected, false);
        }
    }
}
