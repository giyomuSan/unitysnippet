﻿using UnityEngine;
using System.Collections;

public class WanderBehaviour : StateMachineBehaviour
{
    public float Speed = 5;
    public float RotationSpeed = 1f;
    public float MaxHeadingChange = 180f;
    public float DirectionChangeTimeInterval = 1;

    Transform thisTransform;
    float heading;
    Vector3 targetRotation;
    float timer;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log(animator.name + " enter wander state..");
        thisTransform = animator.transform;

        heading = Random.Range(0f, 360f);
        thisTransform.eulerAngles = new Vector3(0f, heading, 0f);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        thisTransform.eulerAngles = Vector3.Slerp(thisTransform.eulerAngles, targetRotation, Time.deltaTime * RotationSpeed);
        var forward = thisTransform.TransformDirection(Vector3.forward);
        thisTransform.position += (forward * Speed) * Time.deltaTime; 

        timer += Time.deltaTime;
        if(timer > DirectionChangeTimeInterval)
        {
            timer = 0f;
            SetNewHeading();
        }
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log(animator.name + " exit wander state..");
    }

    void SetNewHeading()
    {
        var floor = Mathf.Clamp(heading - MaxHeadingChange, 0f, 360f);
        var ceil = Mathf.Clamp(heading + MaxHeadingChange, 0f, 360f);
        heading = Random.Range(floor, ceil);
        targetRotation = new Vector3(0f, heading, 0f);
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
