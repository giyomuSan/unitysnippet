﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class VertexBlendingTest : MonoBehaviour
{

    public GameObject Bone0;
    public GameObject Bone1;

	void Update ()
    {
        if(Bone0 != null)
        {
            GetComponent<Renderer>().sharedMaterial.SetMatrix("_Trafo0", Bone0.GetComponent<Renderer>().localToWorldMatrix);
        }	

        if(Bone1 != null)
        {
            GetComponent<Renderer>().sharedMaterial.SetMatrix("_Trafo1", Bone1.GetComponent<Renderer>().localToWorldMatrix);
        }

        if(Bone0 !=null && Bone1 != null)
        {
            transform.position = 0.5f * (Bone0.transform.position + Bone1.transform.position);
            transform.rotation = Bone0.transform.rotation;
        }

	}
}
