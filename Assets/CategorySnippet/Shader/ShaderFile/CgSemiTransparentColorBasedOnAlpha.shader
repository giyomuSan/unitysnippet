﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Study/CgSemiTransparentColorBasedOnAlpha"
{
	Properties
	{
		_MainTex("RGBA Texture Image", 2D) = "white" {}
		_TextureFactor("Opaque Color Factor", Float) = 0.5
		_AlphaThresold("Alpha Thresold Factor", Range(0,1)) = 0.5
	}
	SubShader
	{
		Tags {"Queue" = "Transparent"}

		Pass
		{
			// First render the back faces
			Cull Front
			// Don't write to depth buffer
			ZWrite Off
			// Blend based on the fragment alpha value
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform sampler2D _MainTex;
			uniform float _TextureFactor;
			uniform float _AlphaThresold;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.tex = input.texcoord;
				output.pos = UnityObjectToClipPos(input.vertex);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				float4 color = tex2D(_MainTex, input.tex.xy);
				if (color.a >_AlphaThresold)
				{
					// opaque back face > opaque dark blue
					color = float4(0.0, 0.0, 0.2, 1.0);
				}
				else
				{
					// transparent back face > semi transparent green
					color = float4(0.0, 0.0, 1.0, 0.3);
				}

				return color;
			}
			ENDCG
		}
		Pass
		{
			// Now render the front face
			Cull Back
			// Don't write to depth buffer
			ZWrite Off
			// Blend based on the fragment alpha value
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform sampler2D _MainTex;
			uniform float _TextureFactor;
			uniform float _AlphaThresold;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.tex = input.texcoord;
				output.pos = UnityObjectToClipPos(input.vertex);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				float4 color = tex2D(_MainTex, input.tex.xy);
				if (color.a > _AlphaThresold)
				{
					// opaque front face > opaque green
					//color = float4(0.0, 1.0, 0.0, 1.0);

					// variation
					color = float4(_TextureFactor * color.r, 1.0 - _TextureFactor * (1.0 - color.g), _TextureFactor * color.b, 1.0);
				}
				else
				{
					// transparent front face > semi transparent dark blue
					color = float4(0.0, 0.0, 1.0, 0.3);
				}

				return color;
			}
			ENDCG
		}
	}
}
