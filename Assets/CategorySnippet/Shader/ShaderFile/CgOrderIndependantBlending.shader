﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Study/CgOrderIndependantBlending"
{
	// this approximate alpha blending more or less..

	Properties
	{
		_Attenuation("Attenuation", Range(0,1)) = 0.3
	}
	SubShader
	{
		Tags { "Queue"="Transparent" }
		
		Pass
		{
			// Draw front and backface
			Cull Off

			// Don't write to depth buffer in order not to occlude other object
			ZWrite Off

			// Multiplicative blending for attenuation by the fragment's alpha
			Blend Zero OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float _Attenuation;

			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(vertexPos);
			}

			float4 frag(void) : COLOR
			{
				// Fourth component handle alpha
				return float4(1.0, 0.0, 0.0, _Attenuation);
			}
			ENDCG
		}

		Pass
		{
			// Draw front and backface
			Cull Off

			// Don't write to depth buffer in order not to occlude other object
			ZWrite Off

			// additive blending
			Blend SrcAlpha One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			uniform float _Attenuation;

			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(vertexPos);
			}

			float4 frag(void) : COLOR
			{
				// Fourth component handle alpha
				return float4(1.0, 0.0, 0.0,_Attenuation);
			}
			ENDCG
		}
	}
}
