﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Platinum-Egg/CGprogram/Transparent/DiffuseLit_Emissive"
{
	Properties
	{
		_Color ("Color", Color) = (0.0, 0.0, 0.0, 0.0)
		_MainTex ("Base (RGBA)", 2D) = ""
	}
	 
	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		LOD 200
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		Pass
		{
			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
		   
		    uniform sampler2D _MainTex;
		    uniform float4 _Color;
		    
		    struct appdata_base
		    {
		        float4 vertex : POSITION;
		        float4 texcoord : TEXCOORD0;
		    };
		   
		    struct v2f
		    {
		        float4 pos : POSITION;
		        float2 texcoord0 : TEXCOORD0;
		    };
		   
		    v2f vert(appdata_base v)
		    {
		        v2f o;
		        o.pos = UnityObjectToClipPos(v.vertex);
		        o.texcoord0 = v.texcoord.xy;
		        
		        return o;
		    }

		    float4 frag(v2f value) : COLOR
		    {
		        float4 mainTex      = tex2D(_MainTex, value.texcoord0) * _Color;
		        //mainTex.a = 0;
		        return mainTex;
		    }
		    
			ENDCG   
		}
	}
}