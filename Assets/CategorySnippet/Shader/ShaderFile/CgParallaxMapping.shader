﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Study/CgParallaxMapping"
{
	Properties
	{
		_BumpMap("Normal Map", 2D) = "bump" {}
		_ParallaxMap("Heightmap (in A)", 2D) = "black" {}
		_Parallax("Max Height", Float) = 0.01
		_MaxTexCoordOffset("Max Texture Coordinate Offset", Float) = 0.01
		_Color("Diffuse Material Color", Color) = (1,1,1,1)
		_SpecColor("Specular Material Color", Color) = (1,1,1,1)
		_Shininess("Shininess", Float) = 10
	}

		// Common codefor all pass of all subshaders
		CGINCLUDE
		#include "UnityCG.cginc"

		uniform float4 _LightColor0;

		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;
		uniform sampler2D _ParallaxMap;
		uniform float4 _ParallaxMap_ST;
		uniform float _Parallax;
		uniform float _MaxTexCoordOffset;
		uniform float4 _Color;
		uniform float4 _SpecColor;
		uniform float _Shininess;

		struct vertexInput
		{
			float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
			float3 normal : NORMAL;
			float4 tangent : TANGENT;
		};

		struct vertexOutput
		{
			float4 pos : SV_POSITION;
			float4 posWorld : TEXCOORD0;
			float4 tex : TEXCOORD1;
			float3 tangentWorld : TEXCOORD2;
			float3 normalWorld : TEXCOORD3;
			float3 binormalWorld : TEXCOORD4;
			float3 viewDirWorld : TEXCOORD5;
			float3 viewDirModeInScaledSurfaceCoords : TEXCOORD6;
		};

		vertexOutput vert(vertexInput input)
		{
			vertexOutput output;

			float4x4 modelMatrix = unity_ObjectToWorld;
			float4x4 modelMatrixInverse = unity_WorldToObject;

			output.tangentWorld = normalize(mul(modelMatrix, float4(input.tangent.xyz, 0.0)).xyz);
			output.normalWorld = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);

			// tangent.w is specific to unity
			output.binormalWorld = normalize(cross(output.normalWorld, output.tangentWorld) * input.tangent.w);

			// appropriate scaled tangent and binormal to map distances from object space to texture space
			float3 binormal = cross(input.normal, input.tangent.xyz) * input.tangent.w;

			float3 viewDirInObjectCoords = mul(modelMatrixInverse, float4(_WorldSpaceCameraPos, 1.0)).xyz - input.vertex.xyz;

			// vector are orthogonal
			float3x3 localSurface2ScaledObjectT = float3x3(input.tangent.xyz, binormal, input.normal);

			// we multiply to with the transpose to multiply with the "inverse" ( apart from scaling )
			output.viewDirModeInScaledSurfaceCoords = mul(localSurface2ScaledObjectT, viewDirInObjectCoords);

			output.posWorld = mul(modelMatrix, input.vertex);
			output.viewDirWorld = normalize(_WorldSpaceCameraPos - output.posWorld.xyz);
			output.tex = input.texcoord;
			output.pos = UnityObjectToClipPos(input.vertex);
			return output;
		}

		// Fragment shader with ambient lighting
		float4 fragWithAmbient(vertexOutput input) : COLOR
		{
			/*parallax mapping : compute height and find offset in texture coordinate for
			the intersection of the view ray with the surface at this height*/

			float height = _Parallax * (-0.5 + tex2D(_ParallaxMap, _ParallaxMap_ST.xy * input.tex.xy + _ParallaxMap_ST.zw).x);
			float2 texCoordOffsets = clamp(height * input.viewDirModeInScaledSurfaceCoords.xy / input.viewDirModeInScaledSurfaceCoords.z, -_MaxTexCoordOffset, +_MaxTexCoordOffset);

			/*normal mapping : In principle we have to normalize binormalWolrd, and normalWorld again,
			however the potential problems are ssmall since we use this matrix only
			to compute "normalDirection" which we normalize anyway*/

			float4 encodedNormal = tex2D(_BumpMap, _BumpMap_ST.xy * input.tex.xy + texCoordOffsets + _BumpMap_ST.zw);
			float3 localCoords = float3(2.0 * encodedNormal.a - 1.0, 2.0 * encodedNormal.g - 1.0, 0.0);
			localCoords.z = sqrt(1.0 - dot(localCoords, localCoords));

			float3x3 local2WorldTranspose = float3x3(input.tangentWorld, input.binormalWorld, input.normalWorld);
			float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));

			// Lighting stuff here .......
			float3 viewDirection = normalize(_WorldSpaceCameraPos - input.posWorld.xyz);
			float3 lightDirection;
			float attenuation;

			// Directional light ?
			if (0.0 == _WorldSpaceLightPos0.w)
			{
				// No attenuation
				attenuation = 1.0;
				lightDirection = normalize(_WorldSpaceLightPos0.xyz);
			}
			else
			{
				float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
				float distance = length(vertexToLightSource);

				// Linear attenuation
				attenuation = 1.0 / distance;
				lightDirection = normalize(vertexToLightSource);
			}

			float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;
			float3 diffuseReflection = attenuation *  _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

			// Specular lighting
			float3 specularReflection;

			// Light source on the wrong side ?
			if (dot(normalDirection, lightDirection) < 0)
			{
				// no specular reflection
				specularReflection = float3(0.0, 0.0, 0.0);
			}
			else
			{
				specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
			}

			return float4(ambientLighting + diffuseReflection + specularReflection, 1.0);
		}

		float4 fragWithoutAmbient(vertexOutput input) : COLOR
		{
			/*parallax mapping : compute height and find offset in texture coordinate for
			the intersection of the view ray with the surface at this height*/

			float height = _Parallax * (-0.5 + tex2D(_ParallaxMap, _ParallaxMap_ST.xy * input.tex.xy + _ParallaxMap_ST.zw).x);
			float2 texCoordOffsets = clamp(height * input.viewDirModeInScaledSurfaceCoords.xy / input.viewDirModeInScaledSurfaceCoords.z, -_MaxTexCoordOffset, +_MaxTexCoordOffset);

			/*normal mapping : In principle we have to normalize binormalWolrd, and normalWorld again,
			however the potential problems are ssmall since we use this matrix only
			to compute "normalDirection" which we normalize anyway*/

			float4 encodedNormal = tex2D(_BumpMap, _BumpMap_ST.xy * input.tex.xy + texCoordOffsets +  _BumpMap_ST.zw);
			float3 localCoords = float3(2.0 * encodedNormal.a - 1.0, 2.0 * encodedNormal.g - 1.0, 0.0);
			localCoords.z = sqrt(1.0 - dot(localCoords, localCoords));

			float3x3 local2WorldTranspose = float3x3(input.tangentWorld, input.binormalWorld, input.normalWorld);
			float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));

			// Lighting stuff here .......
			float3 viewDirection = normalize(_WorldSpaceCameraPos - input.posWorld.xyz);
			float3 lightDirection;
			float attenuation;

			// Directional light ?
			if (0.0 == _WorldSpaceLightPos0.w)
			{
				// No attenuation
				attenuation = 1.0;
				lightDirection = normalize(_WorldSpaceLightPos0.xyz);
			}
			else
			{
				float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
				float distance = length(vertexToLightSource);

				// Linear attenuation
				attenuation = 1.0 / distance;
				lightDirection = normalize(vertexToLightSource);
			}

			float3 diffuseReflection = attenuation *  _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

			// Specular lighting
			float3 specularReflection;

			// Light source on the wrong side ?
			if (dot(normalDirection, lightDirection) < 0)
			{
				// no specular reflection
				specularReflection = float3(0.0, 0.0, 0.0);
			}
			else
			{
				specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);
			}

			return float4(diffuseReflection + specularReflection, 1.0);
		}
		ENDCG

		SubShader
		{
			Pass
			{
				// Pass for ambient light and first light source
				Tags {"LightMode" = "ForwardBase"}

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment fragWithAmbient
				ENDCG
			}

			Pass
			{
				// Pass for ambient light and first light source
				Tags{ "LightMode" = "ForwardAdd" }

				// additive blending
				Blend One One

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment fragWithoutAmbient
				ENDCG
			}
		}
	
}
