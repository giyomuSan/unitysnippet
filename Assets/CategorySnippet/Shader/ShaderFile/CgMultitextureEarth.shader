﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Study/CgMultitextureEarth"
{
	Properties
	{
		_DecalTex("Day Time Earth", 2D) = "white" {}
		_MainTex ("Night Time Earth", 2D) = "white" {}
		_Color("Night Time Color Filter", Color) = (1,1,1,1)
	}
		SubShader
		{
			Tags { "LightMode" = "ForwardBase" }

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"
				uniform float4 _LightColor0;
				uniform sampler2D _MainTex;
				uniform sampler2D _DecalTex;
				uniform float4 _Color;

				struct vertexInput
				{
					float4 vertex : POSITION;
					float3 normal : NORMAL;
					float4 texcoord : TEXCOORD0;
				};

				struct vertexOutput
				{
					float4 pos : SV_POSITION;
					float4 tex : TEXCOORD0;
					float levelOfLighting : TEXCOORD1;
				};

				vertexOutput vert(vertexInput input)
				{
					vertexOutput output;

					float4x4 modelMatrix = unity_ObjectToWorld;
					float4x4 modelMatrixInverse = unity_WorldToObject;

					float3 normalDirection = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
					float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

					output.levelOfLighting = max(0.0, dot(normalDirection, lightDirection));
					output.tex = input.texcoord;
					output.pos = UnityObjectToClipPos(input.vertex);
					return output;
				}

				float4 frag(vertexOutput input) : COLOR
				{
					float4 nightTimeColor = tex2D(_MainTex, input.tex.xy);
					float4 dayTimeColor = tex2D(_DecalTex, input.tex.xy);

					return lerp(nightTimeColor, dayTimeColor, input.levelOfLighting);
				}
			
			ENDCG
		}
	}

}
