﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Study/CgPerVertexDiffuseLighting"
{
	Properties
	{
		_Color("Diffuse Material Color", Color) = (1,1,1,1)
		_SpecColor("Specular Material Color", Color) = (1,1,1,1)
		_Shiness("Shiness", Float) = 10
	}
	SubShader
	{
		Pass
		{
			Tags{"LightMode" = "ForwardBase"}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			// Color of the light source from Lighting.cginc
			uniform float4 _LightColor0;

			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shiness;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};

			
			vertexOutput vert (vertexInput input)
			{
				vertexOutput output;

				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 modelMatrixInverse = unity_WorldToObject;

				float3 normalDirection = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
				float3 viewDirection = normalize(_WorldSpaceCameraPos - mul(modelMatrix, input.vertex).xyz);

				float3 lightDirection;
				float attenuation;

				// Directional light ?
				if (0.0 == _WorldSpaceLightPos0.w)
				{
					// No attenuation
					attenuation = 1.0;
					lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				}
				else
				{
					float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - mul(modelMatrix, input.vertex).xyz;
					float distance = length(vertexToLightSource);

					// Linear attenuation
					attenuation = 1.0 / distance;
					lightDirection = normalize(vertexToLightSource);
				}

				float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;
				float3 diffuseReflection = attenuation *  _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

				// Specular lighting
				float3 specularReflection;

				// Light source on the wrong side ?
				if (dot(normalDirection, lightDirection) < 0)
				{
					// no specular reflection
					specularReflection = float3(0.0, 0.0, 0.0);
				}
				else
				{
					specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shiness);
				}

				output.col = float4(ambientLighting + diffuseReflection + specularReflection, 1.0);
				output.pos = UnityObjectToClipPos(input.vertex);
				return output;
			}
			
			float4 frag (vertexOutput input) : COLOR
			{
				return input.col;
			}
			ENDCG
		}

		Pass
		{
			// Pass for additional light source
			Tags{ "LightMode" = "ForwardAdd" }

			// Additive blending
			Blend One One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			// Color of the light source from Lighting.cginc
			uniform float4 _LightColor0;

			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shiness;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};


			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 modelMatrixInverse = unity_WorldToObject;

				float3 normalDirection = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
				float3 viewDirection = normalize(_WorldSpaceCameraPos - mul(modelMatrix, input.vertex).xyz);

				float3 lightDirection;
				float attenuation;

				// Directional light ?
				if (0.0 == _WorldSpaceLightPos0.w)
				{
					// No attenuation
					attenuation = 1.0;
					lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				}
				else
				{
					float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - mul(modelMatrix, input.vertex).xyz;
					float distance = length(vertexToLightSource);

					// Linear attenuation
					attenuation = 1.0 / distance;
					lightDirection = normalize(vertexToLightSource);
				}

				float3 diffuseReflection = attenuation * _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

				// Specular lighting
				float3 specularReflection;

				// Light source on the wrong side ?
				if (dot(normalDirection, lightDirection) < 0)
				{
					// no specular reflection
					specularReflection = float3(0.0, 0.0, 0.0);
				}
				else
				{
					specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shiness);
				}

				output.col = float4(diffuseReflection + specularReflection, 1.0);
				output.pos = UnityObjectToClipPos(input.vertex);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				return input.col;
			}
				ENDCG
		}
	}
	Fallback "Diffuse"
}
