﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Study/CgGem"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_ReflectTex("Reflection Texture", Cube) = ""
		_RefractionTex("Refraction Texture", Cube) = ""
	}
		SubShader
	{
		Tags { "Queue" = "Transparent" }

		Pass
		{
			Cull Front
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float4 _Color;
			samplerCUBE _ReflectTex;
			samplerCUBE _RefractTex;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normale : NORMAL;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float3 uv : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.pos = UnityObjectToClipPos(input.vertex);

				// TexGen CubeReflect : reflect view direction along the normal, in view space
				float3 viewDir = normalize(ObjSpaceViewDir(input.vertex));
				output.uv = -reflect(-viewDir, input.normale);
				output.uv = mul(UNITY_MATRIX_MV, float4(output.uv, 0));

				return output;
			}

			half4 frag(vertexOutput input) : SV_Target
			{
				float4 col = texCUBE(_RefractTex, input.uv) * _Color;
				col.a = texCUBE(_ReflectTex, input.uv) - 0.5f;
				return col;
			}
			ENDCG
		}
		Pass
		{
			Cull Back
			ZWrite On
			Blend One One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float4 _Color;
			samplerCUBE _ReflectTex;
			samplerCUBE _RefractTex;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normale : NORMAL;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float3 uv : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.pos = UnityObjectToClipPos(input.vertex);

				// TexGen CubeReflect : reflect view direction along the normal, in view space
				float3 viewDir = normalize(ObjSpaceViewDir(input.vertex));
				output.uv = -reflect(-viewDir, input.normale);
				output.uv = mul(UNITY_MATRIX_MV, float4(output.uv, 0));

				return output;
			}

			half4 frag(vertexOutput input) : SV_Target
			{
				float4 col = texCUBE(_RefractTex, input.uv) * _Color + texCUBE(_ReflectTex, input.uv);
				col.a += texCUBE(_ReflectTex, input.uv) - 0.5f;
				return col;
			}
			ENDCG
		}
	}

	FallBack "Diffuse"
}
