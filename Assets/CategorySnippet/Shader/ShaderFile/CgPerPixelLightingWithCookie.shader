﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'
// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Study/CgPerPixelLightingWithCookie"
{
	Properties
	{
		_Color("Diffuse Material Color", Color) = (1,1,1,1)
		_SpecColor("Specular Material Color", Color) = (1,1,1,1)
		_Shiness("Shiness", Float) = 10
	}
	SubShader
	{
		Pass
		{
			Tags{"LightMode" = "ForwardBase"}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			// Color of the light source from Lighting.cginc
			uniform float4 _LightColor0;

			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shiness;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 posWorld : TEXCOORD0;
				float3 normalDir : TEXCOORD1;
			};

			
			vertexOutput vert (vertexInput input)
			{
				vertexOutput output;

				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 modelMatrixInverse = unity_WorldToObject;

				output.posWorld = mul(modelMatrix, input.vertex);
				output.normalDir = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
				output.pos = UnityObjectToClipPos(input.vertex);
				return output;
			}
			
			float4 frag (vertexOutput input) : COLOR
			{
				float3 normalDirection = normalize(input.normalDir);
				float3 viewDirection = normalize(_WorldSpaceCameraPos - input.posWorld.xyz);
				float3 lightDirection;
				float attenuation;

				// Directional light ?
				if (0.0 == _WorldSpaceLightPos0.w)
				{
					// No attenuation
					attenuation = 1.0;
					lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				}
				else
				{
					float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
					float distance = length(vertexToLightSource);

					// Linear attenuation
					attenuation = 1.0 / distance;
					lightDirection = normalize(vertexToLightSource);
				}

				float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;
				float3 diffuseReflection = attenuation *  _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

				// Specular lighting
				float3 specularReflection;

				// Light source on the wrong side ?
				if (dot(normalDirection, lightDirection) < 0)
				{
					// no specular reflection
					specularReflection = float3(0.0, 0.0, 0.0);
				}
				else
				{
					specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shiness);
				}

				return float4(ambientLighting + diffuseReflection + specularReflection, 1.0);
			}
			ENDCG
		}

		Pass
		{
			// Pass for additional light source
			Tags{ "LightMode" = "ForwardAdd" }

			// Additive blending
			Blend One One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			// Color of the light source from Lighting.cginc
			uniform float4 _LightColor0;
			
			// from world to light space ( from AutoLight.cginc )
			uniform float4x4 unity_WorldToLight;

			// cookie alpha ( from AutoLight.cginc )
			uniform sampler2D _LightTexture0;

			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shiness;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 posWorld : TEXCOORD0;
				float4 posLight : TEXCOORD1;
				float3 normalDir : TEXCOORD2;
			};


			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				float4x4 modelMatrix = unity_ObjectToWorld;
				float4x4 modelMatrixInverse = unity_WorldToObject;

				output.posWorld = mul(modelMatrix, input.vertex);
				output.posLight = mul(unity_WorldToLight, output.posWorld);
				output.normalDir = normalize(mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
				output.pos = UnityObjectToClipPos(input.vertex);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				float3 normalDirection = normalize(input.normalDir);
				float3 viewDirection = normalize(_WorldSpaceCameraPos - input.posWorld.xyz);
				float3 lightDirection;
				float attenuation;

				// Directional light ?
				if (0.0 == _WorldSpaceLightPos0.w)
				{
					// No attenuation
					attenuation = 1.0;
					lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				}
				else
				{
					float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
					float distance = length(vertexToLightSource);

					// Linear attenuation
					attenuation = 1.0 / distance;
					lightDirection = normalize(vertexToLightSource);
				}

				float3 diffuseReflection = attenuation *  _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

				// Specular lighting
				float3 specularReflection;

				// Light source on the wrong side ?
				if (dot(normalDirection, lightDirection) < 0)
				{
					// no specular reflection
					specularReflection = float3(0.0, 0.0, 0.0);
				}
				else
				{
					specularReflection = attenuation * _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _Shiness);
				}

				float cookieAttenuation = 1.0;
				if (0.0 == _WorldSpaceLightPos0.w)
				{
					cookieAttenuation = tex2D(_LightTexture0, input.posLight.xy).a;
				}
				else if(1.0 != unity_WorldToLight[3][3])
				{
					cookieAttenuation = tex2D(_LightTexture0, input.posLight.xy / input.posLight.w + float2(0.5, 0.5)).a;
				}

				return float4(cookieAttenuation * (diffuseReflection + specularReflection), 1.0);
			}
			ENDCG
		}
	}
	Fallback "Specular"
}
