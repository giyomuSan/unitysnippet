﻿Shader "Study/CgShaderForVertexBlending"
{
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			// uniform set by script
			uniform float4x4 _Trafo0;
			uniform float4x4 _Trafo1;

			struct vertexInput
			{
				float4 vertex : POSITION;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				// Depends on the mesh
				float weight0 = input.vertex.z + 0.5;

				float4 blendedVertex = weight0 * mul(_Trafo0, input.vertex) + (1.0 - weight0) * mul(_Trafo1, input.vertex);
				output.pos = mul(UNITY_MATRIX_VP, blendedVertex);

				// Visualize weight0 as red and weight1 as green
				output.col = float4(weight0, 1.0 - weight0, 0.0, 1.0);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				return input.col;
			}
			ENDCG
		}
	}
}
