﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Study/Cutaway"
{
	Properties
	{
		_Thresold("Y axis thresold", Range(-0.5, 0.5)) = 0.5	
	}

	SubShader
	{
		Pass
		{
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct vertexInput
			{
				float4 vertex : POSITION;
			};
			
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 posInObjectCoord : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.pos = UnityObjectToClipPos(input.vertex);
				output.posInObjectCoord = input.vertex;

				return output;
			}

			uniform float _Thresold;

			float4 frag(vertexOutput input) : COLOR
			{
				if (input.posInObjectCoord.y > _Thresold)
				{
					// drop the fragment if coordinate y > 0
					discard;
				}
				
				return float4(0.0, 1.0, 0.0, 1.0);
			}
			ENDCG
		}
	}
}
