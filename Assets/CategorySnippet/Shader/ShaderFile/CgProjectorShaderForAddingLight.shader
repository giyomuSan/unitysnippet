﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'

Shader "Study/CgProjectorShaderForAddingLight"
{
	Properties
	{
		_ShadowTex("Projected Image", 2D) = "white" {}
	}
		SubShader
	{
		Pass
		{
			// add color of shadow texture to the color in the frame buffer
			Blend one One

			// don't change depth
			ZWrite Off

			// avoir depth fighting
			Offset -1, -1

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			// user specified properties
			// transformation matrix from object space to projector space
			uniform sampler2D _ShadowTex;

			// projector specific uniform
			uniform float4x4 unity_Projector;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normale : NORMAL;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				// position in projector space
				float4 posProj : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.posProj = mul(unity_Projector, input.vertex);
				output.pos = UnityObjectToClipPos(input.vertex);
				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				if (input.posProj.w > 0.0)
				{
					// front of projector
					return tex2D(_ShadowTex, input.posProj.xy / input.posProj.w);
				}
				else
				{
					return float4(0.0, 0.0, 0.0, 0.0);
				}
			}
			ENDCG
		}
	}
}
