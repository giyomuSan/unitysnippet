﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Study/CgBlending"
{
	SubShader
	{
		Tags { "Queue" = "Transparent" }

		Pass
		{
			// first pass render only back faces , the inside
			Cull Front

			// don't write to depth buffer
			ZWrite Off

			// Use alpha blending
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(vertexPos);
			}

			float4 frag(void) : COLOR
			{
				// Fourth component handle alpha
				return float4(1.0, 0.0, 0.0, 0.3);
			}
			
			ENDCG
		}

		Pass
		{
			// seconds pass render only back faces , the inside
			Cull Back

			// don't write to depth buffer
			ZWrite Off

			// Use alpha blending
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(vertexPos);
			}

			float4 frag(void) : COLOR
			{
				// Fourth component handle alpha
				return float4(0.0, 1.0, 0.0, 0.3);
			}
			ENDCG
		}
	}
}
