﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

 Shader "Study/RgbCubeShader"
{
	SubShader
	{
		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			// For multiple vertex output parameters an output structure is defined
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : TEXCOORD0;
			};

			// Vertex shader
			vertexOutput vert(float4 vertexPos: POSITION)
			{
				vertexOutput output;

				output.pos = UnityObjectToClipPos(vertexPos);

				// We add 0.5 to x,y,z coordinates because the cube coordinates
				// are between -0.5 and 0.5 but we need them between 0.0 and 1.0
				output.col = vertexPos + float4(0.5, 0.5, 0.5, 0.0);
				return output;
			}

			// Fragment shader
			float4 frag(vertexOutput input) : COLOR
			{
				// return our gamut color
				return input.col;

				// transform to gray scale value (red+green+blue)/3 in all 3 color components
				float g = (input.col.r + input.col.g + input.col.b) / 3.0;
				return float4(g, g, g, 1.0);

				// relative luminance value 0.21red+0.72green+0.07blue in all 3 color components
				float l = (0.21 * input.col.r + 0.72 * input.col.g + 0.07 * input.col.b);
				return float4(l, l, l, 1.0);
				
			}

			ENDCG
		}
	}
	
}
