﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Study/CgAdditiveBlending"
{
	SubShader
	{
		// Draw after all opaque geometry has been drawn
		Tags{"Queue" = "Transparent"}

		Pass
		{
			// Draw front and backface
			Cull Off

			// Don't write to depth buffer in order not to occlude other object
			ZWrite Off

			// additive blending
			Blend SrcAlpha One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float4 vert(float4 vertexPos : POSITION) : SV_POSITION
			{
				return UnityObjectToClipPos(vertexPos);
			}

			float4 frag(void) : COLOR
			{
				// Fourth component handle alpha
				return float4(1.0, 0.0, 0.0, 0.3);
			}
			ENDCG
		}
	}
}
