﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Study/DebugShader"
{
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : TEXCOORD0;
			};

			vertexOutput vert(appdata_full input)
			{
				vertexOutput output;

				output.pos = UnityObjectToClipPos(input.vertex);
				output.col = input.texcoord;

				// importance to focus on one color only
				output.col = float4(input.texcoord.x, 0.0, 0.0, 1.0);
				output.col = float4(0.0, input.texcoord.y, 0.0, 1.0);

				// normal
				output.col = float4((input.normal + float3(1.0, 1.0, 1.0)) / 2.0, 1.0);

				// some various value
				//output.col = input.texcoord - float4(1.5, 2.3, 1.1, 0.0);


				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				return input.col;
			}
			ENDCG
		}
	}
}
