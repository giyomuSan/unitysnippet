﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace BTUnityImpl
{
    [System.Serializable]
    public abstract class Node
    {
        public delegate NodeStates NodeReturn();

        protected NodeStates nodeState;
        public NodeStates NodeState { get { return nodeState; } }

        public Node() { }

        public abstract NodeStates Evaluate();
    }

    public class Selector : Node
    {
        protected List<Node> nodes = new List<Node>();
        
        public Selector(List<Node> nodes)
        {
            this.nodes = nodes;
        }

        public override NodeStates Evaluate()
        {
            foreach(var n in nodes)
            {
                switch(n.Evaluate())
                {
                    case NodeStates.Failure:
                        continue;
                    case NodeStates.Success:
                        nodeState = NodeStates.Success;
                        return nodeState;
                    case NodeStates.Running:
                        nodeState = NodeStates.Running;
                        return nodeState;
                    default:
                        continue;
                }
            }

            nodeState = NodeStates.Failure;
            return nodeState;
        }
    }

    public class Sequence : Node
    {
        private List<Node> nodes = new List<Node>();

        public Sequence(List<Node> nodes)
        {
            this.nodes = nodes;
        }

        public override NodeStates Evaluate()
        {
            bool anyChildRunning = false;

            foreach(var n in nodes)
            {
                switch(n.Evaluate())
                {
                    case NodeStates.Failure:
                        nodeState = NodeStates.Failure;
                        return nodeState;
                    case NodeStates.Success:
                        continue;
                    case NodeStates.Running:
                        anyChildRunning = true;
                        continue;
                    default:
                        nodeState = NodeStates.Success;
                        return nodeState;
                }
            }

            nodeState = anyChildRunning ? NodeStates.Running : NodeStates.Success;
            return nodeState;
        }
    }

    public enum NodeStates
    {
        Success,
        Failure,
        Running
    }
}
