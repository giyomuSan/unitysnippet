﻿using UnityEngine;
using System.Collections;

public class RandomPlaceObject : MonoBehaviour
{
    public GameObject[] ObjectList;
    public int ObjectNumberToPlace;
    public float Radius = 1;
    public float Height;

	void Start ()
    {
	    for(int i = 0; i < ObjectNumberToPlace; i++)
        {
            // pick a random object in the list
            var index = Random.Range(0, ObjectList.Length);

            //  pick a random point
            var p = Random.insideUnitSphere * Radius;
            var pos = new Vector3(p.x, Height, p.z);

            // instantiate our object at position
            Instantiate(ObjectList[index], pos, Quaternion.identity);
        }
	}
}
