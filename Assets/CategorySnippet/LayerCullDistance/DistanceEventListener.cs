﻿using UnityEngine;
using UnityEngine.UI;

public class DistanceEventListener : MonoBehaviour
{

	void OnEnable ()
    {
        LayerObjectEventTrigger.Event.AddListener(UpdateText);
	}

    void OnDisable()
    {
        LayerObjectEventTrigger.Event.RemoveListener(UpdateText);
    }

    void UpdateText(string message)
    {
        var textObj = GetComponent<Text>();
        if(textObj)
        {
            textObj.text = message;
        }
    }
}
