﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class LayerCullTest : MonoBehaviour
{
    public float FarDistance;
    public float MidDistance;
    public float CloseDistance;

    public bool UseSphericalCull;
    private Camera cam;

	void Start ()
	{
        cam = GetComponent<Camera>();

        float[] distances = new float[32];

        // layer index to test 
        var far = LayerMask.NameToLayer("Far");
        var between = LayerMask.NameToLayer("Between");
        var close = LayerMask.NameToLayer("Close");

        // set distance cull
        distances[far] = FarDistance;
        distances[between] = MidDistance;
        distances[close] = CloseDistance;

        // set cull mode
        cam.layerCullSpherical = UseSphericalCull;

        // Apply that to our camera
        cam.layerCullDistances = distances;
    }

    void Update()
    {
		// Only to test the difference at runtime
        cam.layerCullSpherical = UseSphericalCull;
    }
}
