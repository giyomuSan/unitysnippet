﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class LayerObjectEventTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public Material BaseMaterial;
    public Material HoverMaterial;
    public Material SelectMaterial;

    public static DistanceMessageEvent Event = new DistanceMessageEvent();

    public void OnPointerClick(PointerEventData eventData)
    {
        ReplaceMaterial(SelectMaterial);

        // send our event if that object is not culled
        if (Event != null && IsVisible())
        {
            Event.Invoke(GetDistanceMessage());
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ReplaceMaterial(HoverMaterial);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ReplaceMaterial(BaseMaterial);
    }

    void ReplaceMaterial(Material mat)
    {
        if(mat != null)
            GetComponent<MeshRenderer>().material = mat;
    }

    bool IsVisible()
    {
        return GetComponent<MeshRenderer>().isVisible;
    }

    String GetDistanceMessage()
    {
        var cameraTransform = Camera.main.gameObject.transform;
        if(cameraTransform)
        {
            var distance = Vector3.Distance(transform.position, cameraTransform.position);
            var message = String.Format("{0} distance to camera is {1} units", gameObject.name, distance);
            return message;
        }

        return string.Empty;
    }
}

public class DistanceMessageEvent : UnityEvent<String> { }
