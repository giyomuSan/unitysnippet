﻿using UnityEngine;

public class FauxGravityAttractor : MonoBehaviour
{
    public float Gravity = -10f;

    public void Attract(Transform body)
    {
        // figure out direction of the player from center of the planet
        var gravityUp = (body.position - transform.position).normalized;
        var bodyUp = body.up;

        // add a force to the body , so it make teh rigibody always head toward the planet
        // this add a force in direction from planet center to the player
        body.GetComponent<Rigidbody>().AddForce(gravityUp * Gravity);

        // rotation
        var targetRotation = Quaternion.FromToRotation(bodyUp, gravityUp) * body.rotation;

        // move toward targetRotation smoothly
        body.rotation = Quaternion.Slerp(body.rotation, targetRotation,  50f * Time.deltaTime);
    }
}
