﻿using UnityEngine;

public class FauxGravityBody : MonoBehaviour
{

    public FauxGravityAttractor attractor;
    private Transform thisTransform;
    private Rigidbody thisRigidbody;

    void Start()
    {
        thisRigidbody = GetComponent<Rigidbody>();

        thisRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        thisRigidbody.useGravity = false;

        thisTransform = transform;
    }


    void Update()
    {
        attractor.Attract(thisTransform);
    }
}
