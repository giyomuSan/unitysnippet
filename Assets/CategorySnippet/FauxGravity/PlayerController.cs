﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float MoveSpeed = 15f;
    private Vector3 moveDirection;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
    }

    void FixedUpdate()
    {
        // move direction need to be in local space
        rb.MovePosition(rb.position + transform.TransformDirection(moveDirection) * MoveSpeed * Time.deltaTime);
    }
}
